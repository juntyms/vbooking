<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Collective\Html\Eloquent\FormAccessible;

class TranspoVehicle extends Model
{
	use FormAccessible;

    protected $table = 'transpo_vehicles';

    protected $fillable = ['transpo_model_id','year','seating_capacity','plate_no','photo','available','km','malkiya_expiry'];

	protected $dates = ['malkiya_expiry'];

	/**
	 * Get the Malkiya Expiration date.
	 *
	 * @param string $date
	 * @return string
	 */
	public function getMalkiyaExpiryAttribute($date)
	{		
		return \Carbon\Carbon::parse($date)->format('m/d/Y');  
	}

	/**
	* 	 Get the Malkiya Expiration for forms.
	* 
	* @param string $date
	* @return string
	*/	
	public function formMalkiyaExpiryAttribute($date)
    {
    	return \Carbon\Carbon::parse($date)->format('Y-m-d');
    }


    public function vehiclemodel()
    {
    	return $this->hasOne('App\TranspoModel','id','transpo_model_id');
    }    
}
