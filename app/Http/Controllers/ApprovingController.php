<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\TranspoApproverUser;
use App\Models\User;
use App\Models\TranspoApprover;
use App\Models\Dept;

class ApprovingController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $depts = Dept::all();
        /*
        $depts = Dept::with(['transpoapprovers'=> function ($query) {

                    $query->orderBy('seq','asc');

                    }])->get();
        */
        //$AppUsers = TranspoApproverUser::orderBy('seq','asc')->get();

        //$AppUserCnt = TranspoApproverUser::count();
        $approverUsers = TranspoApproverUser::all();
//dd($approverUsers);
        
        return view('transpo.approving.index')
                ->with('approverUsers',$approverUsers)
               // ->with('AppUsers',$AppUsers)
               // ->with('AppUserCnt',$AppUserCnt)
                ->with('depts',$depts)
                ->with('nav',5); 
    }

    public function add()
    {
        $users = User::where('resigned_at','0000-00-00 00:00:00')
                        ->where('isStaff',1)
                        ->select(\DB::Raw('CONCAT(fullname," ( ", email ," )") as fullname'), 'id')
                        ->pluck('fullname','id');

        $depts = Dept::pluck('name','id');

        $roles = TranspoApprover::pluck('name','id');

        return view('transpo.approving.add')
                ->with('users',$users)
                ->with('roles',$roles)
                ->with('depts',$depts)
                ->with('nav',5);
    }

    public function save(Request $request)
    {
        $seq = TranspoApproverUser::count();

        $cnt = $seq + 1;

        $request['seq'] = $cnt;

        TranspoApproverUser::create($request->all());

        \Flash::success('Role Assignment Added');
        
        return redirect()->route('transpo.approving.index');
    }

    public function delete($id)
    {
        $approverUser = TranspoApproverUser::findOrfail($id);

        $approverUser->delete();

        \Flash::error('Role Assignment Deleted');

        return redirect()->route('transpo.approving.index');

    }

    
}
