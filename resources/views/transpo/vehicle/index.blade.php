@extends('transpo.base')

@section('breadcrumbs')
<section class="content-header">
  <h1>
    Tranporation System
    <small>Dashboard Panel</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>    
    <li><a href="#">Vehicles</a></li>    
  </ol>
</section>
@endsection

@section('maincontent')
<section class="content">
	<div class="box box-solid box-warning">
	  <div class="box-header with-border">
	    <h3 class="box-title">Vehicles</h3>
	    <div class="box-tools pull-right">
	      <!-- Buttons, labels, and many other things can be placed here! -->
	      <!-- Here is a label for example -->
	      
	    </div><!-- /.box-tools -->
	  </div><!-- /.box-header -->
	  <div class="box-body">
	    <a href="{!! URL::route('transpo.vehicle.add') !!}" class="btn btn-app"> <i class="fa fa-car" aria-hidden="true"></i> Add Vehicle</a>
	    <a href="{!! URl::route('transpo.violationtype.index') !!}" class="btn btn-app"> <i class="fa fa-window-restore" aria-hidden="true"></i> Violation Types</a>
		<table class="table table-bordered">
			<tr class="bg-orange">
				<th>SN</th>
				<th>Make</th>
				<th>Model</th>
				<th>Year</th>
				<th>Seating Capacity</th>
				<th>Plate No</th>
				<th>Malkiya Expiration</th>
				<th>Availability</th>
				<th>Photo</th>
				<th>Details</th>
			</tr>
			@php
				$sn=1;
			@endphp
			@foreach($vehicles as $vehicle)
			<tr>
				<td>{!! $sn++ !!}</td>
				<td>{!! $vehicle->vehiclemodel->modelmake->name !!}</td>
				<td>{!! $vehicle->vehiclemodel->name !!}</td>
				<td>{!! $vehicle->year !!}</td>
				<td>{!! $vehicle->seating_capacity !!}</td>
				<td>{!! $vehicle->plate_no !!}</td>
				<td>@if (isset($vehicle->malkiya_expiry)) {!! \Carbon\Carbon::parse($vehicle->malkiya_expiry)->format('d-M-Y') !!} @endif</td>
				<td>@if ($vehicle->available == 1) Available @else Not Available @endif</td>
				<td><a href="{!! asset($vehicle->photo) !!}" target="_blank">View </a></td>
				<td><a href="{!! URL::route('transpo.vehicle.details', $vehicle->id) !!}" class="btn btn-xs btn-info"> Details</a>
					<a href="{!! URL::route('transpo.vehicle.edit', $vehicle->id) !!}" class="btn btn-xs btn-primary"> Edit</a>
				</td>
			</tr>
			@endforeach
		</table>
	  </div><!-- /.box-body -->
	  <div class="box-footer">
	    List of Vehicles
	  </div><!-- box-footer -->
	</div><!-- /.box -->

	
</section>
@endsection