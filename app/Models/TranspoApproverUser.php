<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TranspoApproverUser extends Model
{
    protected $table = 'transpo_approver_users';

    protected $fillable = ['transpo_approver_id','user_id','dept_id'];

    public function role()
    {
    	return $this->belongsTo('App\TranspoApprover','transpo_approver_id','id');
    }

    public function user()
    {
    	return $this->belongsTo('App\User','user_id','id');
    }
}
