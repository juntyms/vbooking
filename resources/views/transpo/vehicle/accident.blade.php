@extends('transpo.base')

@section('breadcrumbs')
<section class="content-header">
  <h1>
    Tranporation System
    <small>Dashboard Panel</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>    
    <li><a href="#">Vehicles</a></li>  
    <li><a href="#">Accident</a></li>    
  </ol>
</section>
@endsection

@section('maincontent')
<section class="content">
	<div class="row">
		<div class="col-md-6">
			<div class="box">
			  <div class="box-header with-border">
			    <h3 class="box-title">Add Accident Record</h3>
			    <div class="box-tools pull-right">
			      <!-- Buttons, labels, and many other things can be placed here! -->
			      <!-- Here is a label for example -->
			      <!-- This will cause the box to collapse when clicked -->
					<button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
			    </div><!-- /.box-tools -->
			  </div><!-- /.box-header -->
			  <div class="box-body">
			    {!! Form::open(['route'=>['transpo.vehicle.postaccident',$id],'class'=>'form-horizontal']) !!}
					<div class="form-group">
						<label for="dateOfAccident" class="control-label col-md-2">Date of Accident</label>
						<div class="col-md-10">
							{!! Form::date('accident_date',null, ['class'=>'form-control']) !!}
						</div>
					</div>
					<div class="form-group">
						<label for="driver" class="control-label col-md-2">Driver</label>
						<div class="col-md-10">
							{!! Form::select('driver_id',$drivers,null, ['class'=>'form-control','placeholder'=>'Select Driver']) !!}
						</div>
					</div>
					<div class="form-group">
						<label for="description" class="control-label col-md-2">Description</label>
						<div class="col-md-10">
							{!! Form::textarea('description',null, ['class'=>'form-control']) !!}
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-offset-2 col-md-10">
							<button class="btn btn-success"> Save</button>	
						</div>
						
					</div>
			    {!! Form::close() !!}
			  </div><!-- /.box-body -->
			  <div class="box-footer">
			    Add accident record
			  </div><!-- box-footer -->
			</div><!-- /.box -->
		</div>
	</div>
	<div class="box">
	  <div class="box-header with-border">
	    <h3 class="box-title">Accident History</h3>
	    <div class="box-tools pull-right">
	      <!-- Buttons, labels, and many other things can be placed here! -->
	      <!-- Here is a label for example -->
	      <!-- This will cause the box to collapse when clicked -->
			<button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
	    </div><!-- /.box-tools -->
	  </div><!-- /.box-header -->
	  <div class="box-body">
	    <table class="table table-bordered">
	    	<tr>
	    		<th>Date of accident</th>
	    		<th>Driver</th>
	    		<th>Driver Email</th>
	    		<th>Description</th>
	    	</tr>
	    	@foreach($accidents as $accident)
	    	<tr>
	    		<td>{!! $accident->accident_date !!}</td>
	    		<td>{!! $accident->driver->fullname !!}</td>
	    		<td>{!! $accident->driver->email !!}</td>
	    		<td>{!! $accident->description !!}</td>
	    	</tr>
	    	@endforeach
	    </table>
	  </div><!-- /.box-body -->
	  <div class="box-footer">
	    List of accident history
	  </div><!-- box-footer -->
	</div><!-- /.box -->
</section>
@endsection