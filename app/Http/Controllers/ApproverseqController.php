<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Dept;
use App\Models\TranspoApproverSeq;
use App\Models\User;
use App\Models\TranspoApprover;

class ApproverseqController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
    	$depts = Dept::with(['transpoapproverseq'=> function ($query) {

                    $query->orderBy('seq','asc');

                    }])->get();

    	return view('transpo.approverseq.index')
    				->with('nav',7)
    				->with('depts',$depts);
    }

    public function up($id)
    {
        // Get the seq number current record
        $curRec = TranspoApproverSeq::findOrfail($id);

        // Seq of the Above record
        $topSeq = $curRec->seq - 1;
        $curSeq = $curRec->seq;

        // get id of the seq above
        $topRec = TranspoApproverSeq::where('seq',$topSeq)->first();   
        
        $curRec->update(['seq'=>$topSeq]);

        $topRec->update(['seq'=>$curSeq]);        

        return redirect()->route('transpo.approver.seq.index');

    }

    public function down($id)
    {
        // Get the seq number current record
        $curRec = TranspoApproverSeq::findOrfail($id);

        // Seq of the Above record
        $downSeq = $curRec->seq + 1;
        $curSeq = $curRec->seq;

        // get id of the seq above
        $topRec = TranspoApproverSeq::where('seq',$downSeq)->first();   
        
        $curRec->update(['seq'=>$downSeq]);

        $topRec->update(['seq'=>$curSeq]);        

        return redirect()->route('transpo.approver.seq.index');
    }

    public function delete($id)
    {
    	$approverSeq = TranspoApproverSeq::findOrfail($id);

        $approverSeq->delete();

        \Flash::error('Approver Sequence Deleted');

        return redirect()->route('transpo.approver.seq.index');
    }

    public function add()
    {

        $depts = Dept::pluck('name','id');

        $roles = TranspoApprover::pluck('name','id');

        return view('transpo.approverseq.add')
                ->with('roles',$roles)
                ->with('depts',$depts)
                ->with('nav',7);
    }

    public function save(Request $request)
    {
    	$seq = TranspoApproverSeq::where('dept_id',$request->dept_id)->count();

        $cnt = $seq + 1;

        $request['seq'] = $cnt;

        TranspoApproverSeq::create($request->all());

        \Flash::success('Role Assignment Added');
        
        return redirect()->route('transpo.approver.seq.index');
    }

}
