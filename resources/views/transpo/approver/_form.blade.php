<div class="form-group">
	<label for="type" class="control-label col-md-2">Type</label>
	<div class="col-md-10">
		{!! Form::text('name',null, ['class'=>'form-control']) !!}
	</div>
</div>
<div class="form-group">
	<div class="col-md-offset-2 col-md-10">
		<button class="btn btn-success"> {!! $submitButton !!}</button>
	</div>
</div>