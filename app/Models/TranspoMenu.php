<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TranspoMenu extends Model
{
    protected $table = 'transpo_menus';
}
