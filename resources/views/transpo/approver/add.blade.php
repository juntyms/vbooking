@extends('transpo.base')

@section('breadcrumbs')
<section class="content-header">
  <h1>
    Tranporation System
    <small>Approver</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Approver</a></li>        
    <li><a href="#">Add</a></li>
  </ol>
</section>
@endsection

@section('maincontent')
<section class="content">
	<div class="box">
	  <div class="box-header with-border">	  	
	    <h3 class="box-title">Add Approver Type</h3>
	    <div class="box-tools pull-right">
	      <!-- Buttons, labels, and many other things can be placed here! -->
	      <!-- Here is a label for example -->
	      
	    </div><!-- /.box-tools -->
	  </div><!-- /.box-header -->
	  <div class="box-body">
	  	{!! Form::open(['route'=>'transpo.approver.save','class'=>'form-horizontal']) !!}
			@include('transpo.approver._form',['submitButton'=>'Save'])
	  	{!! Form::close() !!}
	  </div><!-- /.box-body -->
	  <div class="box-footer">
	    
	  </div><!-- box-footer -->
	</div><!-- /.box -->
</section>
@endsection