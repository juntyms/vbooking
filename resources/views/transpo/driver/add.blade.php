@extends('transpo.base')

@section('breadcrumbs')
<section class="content-header">
  <h1>
    Tranporation System
    <small>Dashboard Panel</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Driver</a></li>    
    <li><a href="#">Add</a></li>    
  </ol>
</section>
@endsection

@section('maincontent')
<section class="content">
	<div class="box">
	  <div class="box-header with-border">	    
	    <div class="box-tools pull-right">
	      <!-- Buttons, labels, and many other things can be placed here! -->
	      <!-- Here is a label for example -->	      
	    </div><!-- /.box-tools -->
	  </div><!-- /.box-header -->
	  <div class="box-body">
	    <table id="emptables" class="table table-bordered">
			<thead>
				<tr>
					<th>Fullname</th>
					<th>Email</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
			@foreach($users as $user)
			<tr>
				<td>{!! $user->name !!}</td>
				<td>{!! $user->email !!}</td>
				<td>{!! Form::open(['route'=>['transpo.driver.save',$user->id]]) !!}
					<button class="btn btn-sm btn-success"> Save</button>
					{!! Form::close() !!}
			</tr>
			@endforeach
			</tbody>
		</table>
	  </div><!-- /.box-body -->
	</div>
	
</section>
@endsection

@section('jscript')
  <script>
    $(function () {
      $('#emptables').DataTable(); //Full Feature
      $('#itemtables2').DataTable({ //Limited Feature
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "autoWidth": false
      });
    });
  </script>
  @endsection