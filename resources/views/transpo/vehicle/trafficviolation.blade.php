@extends('transpo.base')

@section('breadcrumbs')
<section class="content-header">
  <h1>
    Tranporation System
    <small>Dashboard Panel</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>    
    <li><a href="{!! URL::route('transpo.index') !!}">Dashboard</a></li>
    <li><a href="{!! URL::route('transpo.vehicle.index') !!}">Vehicles</a></li>  
    <li><a href="#">Traffic Violation</a></li>    
  </ol>
</section>
@endsection

@section('maincontent')
<section class="content">
  {!! Form::open(['class'=>'form-horizontal']) !!}
    <div class="form-group">
      <label for="Date" class="control-label col-md-2">Date</label>
      <div class="col-md-3">
        {!! Form::date('violation_date',null, ['class'=>'form-control']) !!}
      </div>
    </div>
    <div class="form-group">  
      <label for="driver" class="control-label col-md-2">Driver</label>
      <div class="col-md-3">
        {!! Form::select('driver_id',$drivers, null , ['class'=>'form-control','placeholder'=>'Select Driver']) !!}
      </div>
    </div>
    <div class="form-group">
      <label for="location" class="control-label col-md-2">Location</label>
      <div class="col-md-3">
        {!! Form::text('location',null, ['class'=>'form-control']) !!}
      </div>
    </div>
    <div class="form-group">
      <label for="violation" class="control-label col-md-2">Type of Violation</label>
      <div class="col-md-3">
        {!! Form::select('transpo_type_of_violation_id',$type_of_violation, null, ['class'=>'form-control','placeholder'=>'Select Type of Violation']) !!}
      </div>
    </div>
    <div class="form-group">
      <label for="amount" class="col-md-2 control-label">Fine (OMR)</label>
      <div class="col-md-3">
        {!! Form::text('amount', null, ['class'=>'form-control']) !!}
      </div>
    </div>
    <div class="form-group">
      <div class="col-md-offset-2 col-md-10">
        <button class="btn btn-success"> <i class="fa fa-floppy-o" aria-hidden="true"></i> Save</button>
        <a href="" class="btn btn-danger"> <i class="fa fa-window-close" aria-hidden="true"></i> Cancel</a>
      </div>
    </div>
  {!! Form::close() !!}
</section>
@endsection