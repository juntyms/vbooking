<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveSeqFromTranspoApproverUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transpo_approver_users', function (Blueprint $table) {
            $table->dropColumn('seq');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transpo_approver_users', function (Blueprint $table) {
            $table->integer('seq');
        });
    }
}
