@extends('transpo.base')

@section('breadcrumbs')
<section class="content-header">
  <h1>
    Tranporation System
    <small>Dashboard Panel</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>    
    <li><a href="{!! URL::route('transpo.vehicle.index') !!}">Vehicles</a></li>  
    <li><a href="#">Edit</a></li>    
  </ol>
</section>
@endsection

@section('maincontent')
<section class="content">
	{!! Form::model($vehicle, ['route'=>['transpo.vehicle.update',$vehicle->id], 'class'=>'form-horizontal']) !!}		
	<div class="form-group">
		{!! Form::label('year','Model Year', ['class'=>'control-label col-md-2']) !!}
		<div class="col-md-10">
			{!! Form::text('year',null, ['class'=>'form-control']) !!}
		</div>
	</div>
	<div class="form-group">
		{!! Form::label('capacity','Seating Capacity', ['class'=>'control-label col-md-2']) !!}
		<div class="col-md-10">
			{!! Form::text('seating_capacity', null, ['class'=>'form-control']) !!}
		</div>
	</div>
	<div class="form-group">
		{!! Form::label('plate_no','Plate No.', ['class'=>'control-label col-md-2']) !!}
		<div class="col-md-10">
			{!! Form::text('plate_no',null, ['class'=>'form-control']) !!}
		</div>
	</div>
	<div class="form-group">
		{!! Form::label('photo','Photo', ['class'=>'control-label col-md-2']) !!}
		<div class="col-md-10">
			{!! Form::text('photo', null, ['class'=>'form-control']) !!}
		</div>
	</div>
	<div class="form-group">
		{!! Form::label('available','Available', ['class'=>'control-label col-md-2']) !!}
		<div class="col-md-10">
			{!! Form::select('available',['1'=>'Available','0'=>'Not Available'],null, ['class'=>'form-control']) !!}
		</div>
	</div>
	<div class="form-group">
		{!! Form::label('malkiya_expiry','Malkiya Expiration', ['class'=>'control-label col-md-2']) !!}
		<div class="col-md-2">
			{!! Form::date('malkiya_expiry', null, ['class'=>'form-control']) !!}
		</div>
	</div>
	<div class="form-group">
		<div class="col-md-offset-2 col-md-10">
			<button> Update</button>
		</div>
	</div>
	{!! Form::close() !!}
</section>
@endsection