@extends('transpo.base')

@section('breadcrumbs')
<section class="content-header">
  <h1>
    Tranporation System
    <small>Dashboard Panel</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Make</a></li>
    <li><a href="#">New</a></li>
  </ol>
</section>
@endsection

@section('maincontent')
<section class="content">
  {!! Form::open(['route'=>'transpo.make.save', 'class'=>'form-horizontal']) !!}
    @include('transpo.make._form',['submitText'=>' Save'])
  {!! Form::close() !!}
</section>
@endsection