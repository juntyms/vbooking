@extends('transpo.base')

@section('breadcrumbs')
<section class="content-header">
  <h1>
    Tranporation System
    <small>Dashboard Panel</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Drivers</a></li>    
  </ol>
</section>
@endsection

@section('maincontent')
<section class="content">
	<div class="box">
	  <div class="box-header with-border">
	    <h3 class="box-title">Drivers List</h3><br />
	    <a href="{!! URL::route('transpo.driver.add') !!}" class="btn btn-app"><i class="fa fa-user-circle-o" aria-hidden="true"></i> Add</a>	    
	  </div><!-- /.box-header -->
	  <div class="box-body">
	    <table class="table table-bordered">
			<tr class="bg-orange">
				<th>SN</th>
				<th>Fullname</th>
				<th>Email</th>
				<th></th>
			</tr>
			@php
				$sn=1;
			@endphp
			@foreach($drivers as $driver)
				<tr>
					<td>{!! $sn++ !!}</td>
					<td>{!! $driver->driver->name !!}</td>
					<td>{!! $driver->driver->email !!}</td>
					<td><a href="{!! URL::route('transpo.driver.remove',$driver->id) !!}"> Remove</a></td>
				</tr>
			@endforeach
		</table>
	  </div><!-- /.box-body -->
	</div>
	
</section>
@endsection