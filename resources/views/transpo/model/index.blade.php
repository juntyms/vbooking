@extends('transpo.base')

@section('breadcrumbs')
<section class="content-header">
  <h1>
    Tranporation System
    <small>Dashboard Panel</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="{!! URL::route('transpo.make.index') !!}">Make</a></li>
    <li><a href="#">Model</a></li>    
  </ol>
</section>
@endsection

@section('maincontent')
<section class="content">
	<a href="{!! URL::route('transpo.model.new',$makeId) !!}" class="btn btn-app"> New Model</a>
	<table class="table table-striped">
		<tr>
			<th>SN</th>
			<th>Make</th>
			<th>Model</th>			
			<th></th>
		</tr>
		@php
			$sn = 1;
		@endphp
		@foreach($models as $model)
		<tr>
			<td>{!! $sn++ !!}</td>
			<td>{!! $model->modelmake->name !!}</td>
			<td>{!! $model->name !!}</td>			
			<td>
				<a href="{!! URL::route('transpo.model.edit',$model->id) !!}" class="btn btn-info btn-xs"> Edit </a>
				<a href="{!! URL::route('transpo.model.delete', $model->id) !!}" class="btn btn-danger btn-xs"> Delete </a>
			</td>
		</tr>
		@endforeach
	</table>
</section>
@endsection