@extends('transpo.base')

@section('breadcrumbs')
<section class="content-header">
  <h1>
    Tranporation System
    <small>Vehicle Release</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Booking</a></li>        
    <li><a href="#">Vehicle</a></li>        
    <li><a href="#">Release</a></li>        
  </ol>
</section>
@endsection

@section('maincontent')
	<section class="content">
		{!! Form::open(['route'=>['tranpo.booking.postrelease',$booking->id],'class'=>'form-horizontal']) !!}
			<div class="form-group">
				<label for="requester" class="col-md-2 control-label">Requested By</label>
				<div class="col-md-10">
					{!! Form::text('requester',$booking->fullname, ['class'=>'form-control', 'readOnly']) !!}
				</div>
			</div>
			@if ($booking->transpo_driver_id == 0)
			<div class="form-group">
				<label for="driver" class="col-md-2 control-label">Select Driver</label>
				<div class="col-md-10">
					{!! Form::select('transpo_driver_id',$users, null , ['class'=>'form-control','placeholder'=>'Select Driver']) !!}
				</div>
			</div>
			@endif
			<div class="form-group">
				<label for="purpose" class="col-md-2 control-label">Purpose</label>
				<div class="col-md-10">
					{!! Form::textarea('purpose',$booking->purpose, ['class'=>'form-control', 'readOnly']) !!}
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-offset-2 col-md-10">
					<button>Release</button>	
				</div>				
			</div>
		{!! Form::close() !!}
	</section>
@endsection