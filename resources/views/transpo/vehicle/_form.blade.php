<div class="form-group">
	{!! Form::label('make','Vehicle Make', ['class'=>'control-label col-md-2']) !!}
	<div class="col-md-10">
		{!! Form::Select('make',$makes, null, ['class'=>'form-control','id'=>'maketype','placeholder'=>'Select Make']) !!}
	</div>
</div>
<div class="form-group">
	{!! Form::label('model','Vehicle Model', ['class'=>'control-label col-md-2']) !!}
	<div class="col-md-10">
		{!! Form::select('transpo_model_id', [], null, ['id'=>'transpomodel','class'=>'form-control']) !!}
	</div>
</div>
<div class="form-group">
	{!! Form::label('year','Model Year', ['class'=>'control-label col-md-2']) !!}
	<div class="col-md-10">
		{!! Form::text('year',null, ['class'=>'form-control']) !!}
	</div>
</div>
<div class="form-group">
	{!! Form::label('capacity','Seating Capacity', ['class'=>'control-label col-md-2']) !!}
	<div class="col-md-10">
		{!! Form::text('seating_capacity', null, ['class'=>'form-control']) !!}
	</div>
</div>
<div class="form-group">
	{!! Form::label('plate_no','Plate No.', ['class'=>'control-label col-md-2']) !!}
	<div class="col-md-10">
		{!! Form::text('plate_no',null, ['class'=>'form-control']) !!}
	</div>
</div>
<div class="form-group">
	{!! Form::label('photo','Photo', ['class'=>'control-label col-md-2']) !!}
	<div class="col-md-10">
		{!! Form::text('photo', null, ['class'=>'form-control']) !!}
	</div>
</div>
<div class="form-group">
	{!! Form::label('available','Available', ['class'=>'control-label col-md-2']) !!}
	<div class="col-md-10">
		{!! Form::select('available',['1'=>'Available','0'=>'Not Available'],null, ['class'=>'form-control']) !!}
	</div>
</div>
<div class="form-group">
	<div class="col-md-offset-2 col-md-10">
		<button>{!! $submitText !!}</button>
	</div>
</div>

@section('jscript')
<script>
	$('#maketype').on('change', function(e) {
		//console.log(e.target.value);

		var makeid = e.target.value;

		$.get('{{ url('transpo') }}/json/make/' + makeid + '/model', function(data) {

			console.log(data);
	        
	        $('#transpomodel').empty();
	        
	        $('#transpomodel').append('<option>... Select Items ...</option>');
	        
	        $.each(data, function(index, subCatObj) {
	            $('#transpomodel').append('<option value='+ subCatObj.id +'>'+ subCatObj.text +'</option>');
	        });

      	});
	});
</script>
@endsection