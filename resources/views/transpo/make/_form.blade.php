<div class="form-group">
	{!! Form::label('name','Make ', ['class'=>'control-label col-md-2']) !!}
	<div class="col-md-10">
		{!! Form::text('name', null, ['class'=>'form-control']) !!}
	</div>
</div>
<div class="form-group">
	<div class="col-md-offset-2 col-md-10">
		<button class="btn btn-primary btn-sm">{!! $submitText !!}</button>
	</div>
</div>