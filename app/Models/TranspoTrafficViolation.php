<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TranspoTrafficViolation extends Model
{
    protected $table = 'transpo_traffic_violations';

    protected $fillable = ['transpo_vehicle_id','driver_id','violation_date','location','transpo_type_of_violation_id','amount','updated_by'];

    public function offense()
    {
    	return $this->hasOne('App\TranspoTypeOfViolations','id','transpo_type_of_violation_id');
    }

    public function driver()
    {
    	return $this->hasOne('App\User','id','driver_id');
    }

    public function updatedBy()
    {
        return $this->hasOne('App\User','id','updated_by');
    }

    public function vehicle()
    {
        return $this->hasOne('App\TranspoVehicle','id','transpo_vehicle_id');
    }
}
