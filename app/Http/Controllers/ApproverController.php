<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\TranspoApprover;

class ApproverController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
    	$transpoApprovers = TranspoApprover::all();    	

    	return view('transpo.approver.index')
    				->with('transpoApprovers',$transpoApprovers)
                    ->with('nav',6);

    }

    public function add()
    {
    	return view('transpo.approver.add')->with('nav',6);;
    }

    public function save(Request $request)
    {
    	TranspoApprover::create($request->all());

    	\Flash::success('Approver Added Successfully');

    	return redirect()->route('transpo.approver.index');
    }

    public function edit($id)
    {
    	$transpoApprover = TranspoApprover::findOrFail($id);

    	return view('transpo.approver.edit')
                ->with('transpoApprover',$transpoApprover)
                ->with('nav',6);;
    }

    public function update(Request $request, $id)
    {
        
    	TranspoApprover::where('id',$id)
    						->update(['name'=>$request->name]);

    	\Flash::success('Approver Type Updated');

    	return redirect()->route('transpo.approver.index');
    	
    }
}
