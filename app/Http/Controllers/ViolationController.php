<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Models\TranspoTypeOfViolations;
use App\Models\User;
use App\Models\TranspoTrafficViolation;


class ViolationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function trafficviolation($id)
    {
    	$type_of_violation = TranspoTypeOfViolations::pluck('name','id');

    	$drivers = User::pluck('fullname','id');

        return view('transpo.vehicle.trafficviolation')
                ->with('nav',3)
                ->with('id',$id)
                ->with('type_of_violation',$type_of_violation)
                ->with('drivers',$drivers);

    }

    public function posttrafficviolation(Request $request, $id)
    {
        $request['updated_by'] = Auth::user()->id;

        $request['transpo_vehicle_id'] = $id;

        TranspoTrafficViolation::create($request->all());

        \Flash::success('Traffic Violation Added');

    	return redirect()->route('transpo.vehicle.details',$id);
    }
}
