@extends('mainlayout')

@section('bodyclass','class="hold-transition skin-blue sidebar-mini"')

@section('breadcrumbs')
<section class="content-header">
  <h1>
    Dashboard
    <small>Control Panel</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{!! URL::route('transpo.index') !!}"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Dashboard</li>
  </ol>
</section>
@endsection

@section('content')

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->

       




		
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-7 connectedSortable">
          <!-- Custom tabs (Charts with tabs)-->
          {{-- @include('main._charts') --}}

          <!-- /.nav-tabs-custom -->

          <!-- Chat box -->
          {{--  @include('main._chatbox')  --}}
          <!-- /.box (chat box) -->

          <!-- TO DO List -->
          {{--  @include('main._todobox')  --}}
          <!-- /.box -->

          <!-- quick email widget -->
          {{-- @include('main._emailwidget') --}}

        </section>
        <!-- /.Left col -->
        <!-- right col (We are only adding the ID to make the widgets sortable)-->
        <section class="col-lg-5 connectedSortable">

          <!-- Map box -->
          {{-- @include('main._mapbox') --}}

          <!-- /.box -->

          <!-- solid sales graph -->
          {{-- @include('main._salesbox') --}}

          <!-- /.box -->

          <!-- Calendar -->
          {{-- @include('main._calendarbox') --}}

          <!-- /.box -->

        </section>
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->

@endsection
