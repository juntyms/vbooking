@extends('transpo.base')

@section('breadcrumbs')
<section class="content-header">
  <h1>
    Tranporation System
    <small>Dashboard Panel</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Request</a></li>
  </ol>
</section>
@endsection

@section('maincontent')
<section class="content">
	<p>Model : {!! $vehicle->vehiclemodel->name !!}</p>
	<p>Year : {!! $vehicle->year !!}</p>
	{!! Form::open(['class'=>'form-horizontal']) !!}
		<div class="form-group">
			<label for="start_date" class="col-md-2 control-label"> Start Date</label>
			<div class="col-md-4 col-sm-12 col-xs-12">
				{!! Form::date('start_date',null, ['class'=>'form-control']) !!}
			</div>
		</div>
		<div class="form-group">
			<label for="end_date" class="col-md-2 control-label"> End Date</label>
			<div class="col-md-4 col-sm-12 col-xs-12">
				{!! Form::date('end_date',null, ['class'=>'form-control']) !!}
			</div>
		</div>
		<div class="form-group">
			<label for="transpo_driver_id" class="col-md-2 control-label">Driver Option</label>
			<div class="col-md-10">
				{!! Form::radio('transpo_driver_id','-1',true) !!} <label for="">With Driver</label>
				{!! Form::radio('transpo_driver_id','0') !!} <label for="">Without Driver</label>
			</div>
		</div>
		<div class="form-group">
			<label for="purpose" class="col-md-2 control-label"> Purpose</label>
			<div class="col-md-10">
				{!! Form::textarea('purpose', null, ['class'=>'form-control']) !!}
			</div>
		</div>
		<div class="form-group">
			<div class="col-md-offset-2 col-md-10">
				<button class="btn btn-success"> Request</button>
			</div>
		</div>
	{!! Form::close() !!}
</section>
@endsection