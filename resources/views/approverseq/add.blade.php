@extends('transpo.base')

@section('breadcrumbs')
<section class="content-header">
  <h1>
    Tranporation System
    <small>Approver Assignment</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Approver</a></li>
    <li><a href="#">Seq</a></li>        
    <li><a href="#">Add</a></li>       
  </ol>
</section>
@endsection

@section('maincontent')
<section class="content">
	<div class="box">
	  <div class="box-header with-border">
	    <h3 class="box-title">Approver Assignment</h3>
	    <div class="box-tools pull-right">
	      <!-- Buttons, labels, and many other things can be placed here! -->
	      <!-- Here is a label for example -->	      
	    </div><!-- /.box-tools -->
	  </div><!-- /.box-header -->
	  <div class="box-body">
	    {!! Form::open(['route'=>'transpo.approver.seq.save','class'=>'form-horizontal']) !!}
			<div class="form-group">
				<label for="Role" class="col-md-2 control-label">Approve Role</label>
				<div class="col-md-10">
					{!! Form::select('transpo_approver_id',$roles, null, ['class'=>'form-control roles','placeholder'=>'Role']) !!}
				</div>
			</div>
			<div class="form-group">
				<label for="depts" class="col-md-2 control-label">Department</label>
				<div class="col-md-10">
					{!! Form::select('dept_id',$depts,null, ['class'=>'form-control','placeholder'=>'Select Department']) !!}
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-offset-2 col-md-2">
					<button class="btn btn-success"> Assign</button>
				</div>
			</div>
	    {!! Form::close() !!}
	  </div><!-- /.box-body -->	  
	</div><!-- /.box -->
</section>
@endsection

@section('jscript')
{!! HTML::script('plugin/select/js/select2.min.js') !!}
<script>
  $(function () {
    //Initialize Select2 Elements
    $(".roles").select2();
  });
</script>
@endsection