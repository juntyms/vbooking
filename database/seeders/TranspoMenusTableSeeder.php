<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class TranspoMenusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('transpo_menus')->delete();
        TranspoMenu::create(['name_en'=>'Make']);
        TranspoMenu::create(['name_en'=>'Vehicle']);
        TranspoMenu::create(['name_en'=>'Drivers']);
        TranspoMenu::create(['name_en'=>'Approver Assignment']);
        TranspoMenu::create(['name_en'=>'Approver Description']);
        TranspoMenu::create(['name_en'=>'Approver Seq']);
        TranspoMenu::create(['name_en'=>'Report']);
        TranspoMenu::create(['name_en'=>'Settings']);
    }
}
