<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTranspoBookingApprovalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transpo_booking_approvals', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedBigInteger('transpo_booking_id');
            $table->foreign('transpo_booking_id')->references('id')->on('transpo_bookings');
            $table->unsignedBigInteger('transpo_approver_id');            
            $table->foreign('transpo_approver_id')->references('id')->on('transpo_approvers');
            $table->integer('approver_id');
            $table->timestamp('approved_date')->nullable();
            $table->string('comments')->nullable();
            $table->unsignedBigInteger('transpo_status_id');
            $table->foreign('transpo_status_id')->references('id')->on('transpo_status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('transpo_booking_approvals');
    }
}
