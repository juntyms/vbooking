<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Models\TranspoBooking;
use App\Http\Requests\transpo\TranspoRejectRequest;
use App\Http\Requests\transpo\TranspoReturnRequest;

class BookingController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function approve($id, $bookingid)
    {
    	// Update 
    	\DB::table('transpo_booking_approvals')
    			->where('id',$id)
                ->update(['transpo_status_id'=>'3',
                            'approver_id'=>Auth::user()->id,
                            'approved_date'=>\Carbon\Carbon::now(),
                            'updated_at'=>\Carbon\Carbon::now()]);

    	// Query remaining approvers
    	$remaining = \DB::table('transpo_booking_approvals')
		    			->where('transpo_booking_id',$bookingid)
		    			->where('approver_id',0)
		    			->orderBy('id','asc')
		    			->first();

		// Check if there are still remaining approver
		//if (count($remaining) > 0) {
        if ($remaining) {
			// set the next approver 
			\DB::table('transpo_booking_approvals')
					->where('id',$remaining->id)
					->update(['approver_id'=>'-1']);

		} else {
			// finalized the booking status
			\DB::table('transpo_bookings')
				->where('id',$bookingid)
				->update(['transpo_status_id'=>'3','updated_at'=>\Carbon\Carbon::now()]);
		}

		\Flash::success('Request Approved');

    	return redirect()->route('transpo.index');
    }


    public function reject($id, $bookingid)
    {
        $booking = TranspoBooking::where('id',$bookingid)->first();

        return view('transpo.booking.reject')
                    ->with('booking',$booking)
                    ->with('id',$id)
                    ->with('nav',1);
    }

    public function postreject($id, $bookingid, TranspoRejectRequest $request)
    {
        // $id - transpo_booking_approvals id
        // $bookingid - transpo_booking id

    	\DB::table('transpo_booking_approvals')
    		->where('id',$id)
    		->update(['transpo_status_id'=>'2',
                        'approver_id'=>Auth::user()->id,
                        'comments'=> $request->reject_reason,
                        'approved_date'=>\Carbon\Carbon::now(),
                        'updated_at'=>\Carbon\Carbon::now()]);

    	\DB::table('transpo_bookings')
    		->where('id',$bookingid)
    		->update(['transpo_status_id'=>'2','updated_at'=>\Carbon\Carbon::now()]);

    	\Flash::error('Request Rejected');

    	return redirect()->route('transpo.index');

    }

    public function release($bookingid)
    {

        $users = \DB::table('users')
                    ->where('isStaff',1)
                    ->where('resigned_at','0000-00-00 00:00:00')
                    ->pluck('fullname','id');

        $booking = \DB::table('transpo_bookings')
                        ->where('transpo_bookings.id',$bookingid)
                        ->leftjoin('users','users.id','=','transpo_bookings.requested_by')
                        ->select('transpo_bookings.id',
                                    'users.fullname',
                                    'transpo_bookings.transpo_driver_id',
                                    'transpo_bookings.start_date',
                                    'transpo_bookings.end_date',
                                    'transpo_bookings.purpose')
                        ->first();

        return view('transpo.booking.release')
                ->with('nav',1)
                ->with('users',$users)
                ->with('booking',$booking);
    }

    public function postrelease($bookingid, Request $request)
    {
        // Check if request Requires Driver
        $booking = \DB::table('transpo_bookings')
                        ->where('id',$bookingid)
                        ->first();

        if ($booking->transpo_driver_id == 0) {
            //Driver is Required
            if ($request->has('transpo_driver_id')) {
                \DB::table('transpo_bookings')
                    ->where('id',$bookingid)
                    ->update(['transpo_driver_id'=>$request->transpo_driver_id,
                            'transpo_status_id'=>'6',
                            'release_date'=> \Carbon\Carbon::now(),
                            'updated_at'=> \Carbon\Carbon::now(),
                            'release_by'=> Auth::user()->id                            
                        ]);
            } else {
                \Flash::error('Please Select Driver');
                return back()->withInput();
            }          
        } else {
            \DB::table('transpo_bookings')
                    ->where('id',$bookingid)
                    ->update(['transpo_status_id'=>'6',
                            'release_date'=> \Carbon\Carbon::now(),
                            'updated_at'=> \Carbon\Carbon::now(),
                            'release_by'=> Auth::user()->id
                        ]);
        }        

        \Flash::success('Vehicle Released');

        return redirect()->route('transpo.index');
    }

    public function return($bookingid)
    {
        $booking = TranspoBooking::findOrFail($bookingid);

        return view('transpo.booking.return')
                    ->with('nav',1)
                    ->with('booking',$booking);
    }

    public function postreturn($bookingid, TranspoReturnRequest $request)
    {
        $time = $request->return_time;
        $date = $request->return_date;
        
        $date_of_return = \Carbon\Carbon::parse($date . $time);

        // Update Transpo_bookings Table
        $booking = TranspoBooking::findOrFail($bookingid);

        $booking->transpo_status_id = 7;

        $booking->return_date = $date_of_return;

        $booking->return_km = $request->return_km;

        $booking->remarks = $request->remarks;

        $booking->accepted_by = Auth::user()->id;

        $booking->save();

        // Update Transpo_vehicles Table

        \DB::table('transpo_vehicles')
            ->where('id',$booking->transpo_vehicle_id)
            ->update(['km'=>$request->return_km]);


        \Flash::success('Vehicle Successfully returned');

        return redirect()->route('transpo.index');

    }
}
