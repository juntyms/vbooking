@extends('transpo.base')

@section('breadcrumbs')
<section class="content-header">
  <h1>
    Tranporation System
    <small>Dashboard Panel</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Request</a></li>
  </ol>
</section>
@endsection

@section('maincontent')
<section class="content">
	<div class="box">
	  <div class="box-header with-border">
	    <h3 class="box-title">Booking Request Details</h3>
	    <div class="box-tools pull-right">
	      <!-- Buttons, labels, and many other things can be placed here! -->
	      <!-- Here is a label for example -->	      
	    </div><!-- /.box-tools -->
	  </div><!-- /.box-header -->
	  <div class="box-body">	  	
	  	<div class="row">
		  	<div class="col-md-4">
		  		<img src="{!! asset($transpobooking->vehicle->photo ) !!}">
		  	</div>
	  		
	  		<div class="col-md-4">
				<dl class="dl-horizontal">
					<dt>Booking ID</dt>
					<dd>{!! $transpobooking->id !!}</dd>
					<dt>Vehicle</dt>
					<dd>{!! $transpobooking->vehicle->vehiclemodel->name !!}</dd>
					<dt>Model</dt>
					<dd>{!! $transpobooking->vehicle->year !!}</dd>
					<dt>Plate No</dt>
					<dd>{!! $transpobooking->vehicle->plate_no !!}</dd>
					<dt>Start Date</dt>
					<dd>{!! \Carbon\Carbon::parse($transpobooking->start_date)->format('M d, Y') !!}</dd>
					<dt>End Date</dt>
					<dd>{!! \Carbon\Carbon::parse($transpobooking->end_date)->format('M d, Y') !!}</dd>
					<dt>Seating Capacity</dt>
					<dd>{!! $transpobooking->vehicle->seating_capacity !!}</dd>
					<dt>Requested By</dt>
					<dd>{!! $transpobooking->requester->fullname !!}</dd>
					<dt>Released By</dt>
					<dd>@if (isset($transpobooking->released_by->fullname )) {!! $transpobooking->released_by->fullname !!} @endif</dd>
				</dl>
			</div>
			<div class="col-md-4">
				<dl class="dl-horizontal">
					<dt>Returned Date & Time</dt>
					<dd>{!! $transpobooking->return_date !!}</dd>
					<dt>Returned Kilometer</dt>
					<dd>{!! $transpobooking->return_km !!}</dd>
					<dt>Recieved By</dt>
					<dd>@if (isset($transpobooking->received_by->fullname)) {!! $transpobooking->received_by->fullname !!} @endif</dd>
					<dt>Received Remarks</dt>
					<dd>{!! $transpobooking->remarks !!}</dd>
				</dl>
			</div>
		</div>
			<div class="table-responsive">
			<table class="table table-bordered">
				<tr class="bg-blue">
					<th>SN</th>
					<th>Approver Role</th>
					<th>Rejected/Approved By</th>
					<th>Status</th>
					<th>Comments</th>
					<th>Email</th>
				</tr>
				@php 
					$sn=1;
				@endphp
				@foreach($transpobooking->approver as $approver)
					<tr
						@if ($approver->transpo_status_id==2) class="bg-danger" @endif
						@if ($approver->transpo_status_id==3) class="bg-success" @endif
					>
						<td>{!! $sn++ !!}</td>
						<td>{!! $approver->approverdesc->name !!}</td>					
						<td>{!! $approver->approver['fullname'] !!}</td>
						<td>{!! $approver->statusdesc->name !!}</td>
						<td>{!! $approver->comments !!}</td>
						<td>{!! $approver->approver['email'] !!}</td>
					</tr>
				@endforeach
			</table>	    
		</div>
	  </div><!-- /.box-body -->
	</div><!-- /.box -->
	
</section>
@endsection