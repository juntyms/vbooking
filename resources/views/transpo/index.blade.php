@extends('transpo.base')

@section('breadcrumbs')
<section class="content-header">
  <h1>
    Tranporation System
    <small>Dashboard Panel</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Dashboard</a></li>
  </ol>
</section>
@endsection

@section('maincontent')
<section class="content">
	<div class="box box-success">
	  <div class="box-header with-border">
	    <h3 class="box-title">Vehicle Requests</h3> <span data-toggle="tooltip" title="{!! count($reservations) !!} Requested" class="badge bg-red">{!! count($reservations) !!}</span>
	    <div class="box-tools pull-right">
	      <!-- Buttons, labels, and many other things can be placed here! -->
	      <!-- Here is a label for example -->
	      
	      <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
	    </div><!-- /.box-tools -->
	  </div><!-- /.box-header -->
	  <div class="box-body">
	  	<div class="table-responsive">
		    <table class="table table-bordered">
		    	<tr>
		    		<th>SN</th>
		    		<th>Requester</th>
		    		<th>Purpose</th>
		    		<th>Vehicle Model</th>
		    		<th>Plate No</th>	    		
		    		<th>Start Date</th>
		    		<th>End Date</th>
		    		<th>Status</th>
		    	</tr>
		    	@php
					$sn=1;
		    	@endphp
		    	@foreach($reservations as $reservation)
		    	<tr
					
					@if ($reservation->transpo_status_id == 2)  class="bg-danger" @endif
					@if ($reservation->transpo_status_id == 3)  class="bg-primary" @endif
					@if ($reservation->transpo_status_id == 6) class="bg-success" @endif
					@if ($reservation->transpo_status_id == 7) class="bg-info" @endif

		    	>
		    		<td>{!! $sn++ !!}</td>
		    		<td>{!! $reservation->requester->fullname !!}</td>
		    		<td>{!! $reservation->purpose !!}</td>
		    		<td>{!! $reservation->vehicle->vehiclemodel->name !!}</td>
		    		<td>{!! $reservation->vehicle->plate_no !!}</td>
		    		<td>{!! \Carbon\Carbon::parse($reservation->start_date)->format('M d, Y') !!}</td>
		    		<td>{!! \Carbon\Carbon::parse($reservation->end_date)->format('M d, Y') !!}</td>
		    		<td><a href="{!! URL::route('transpo.requestdetail',$reservation->id) !!}">{!! $reservation->status->name !!}</a>						
		    		</td>
		    	</tr>
		    	@endforeach
		    </table>
		</div>
	  </div><!-- /.box-body -->
	  <div class="box-footer">
	    List of Vehicle Request
	  </div><!-- box-footer -->
	</div><!-- /.box -->
	<div class="box box-warning collapsed-box">
	  <div class="box-header with-border">	   
	  	<h3 class="box-title">Select Vehicle</h3> 
	    <div class="box-tools pull-right">
	      <!-- Buttons, labels, and many other things can be placed here! -->
	      <!-- Here is a label for example -->
	      <!-- This will cause the box to collapse when clicked -->
			<button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-plus"></i></button>
	    </div><!-- /.box-tools -->
	  </div><!-- /.box-header -->
	  <div class="box-body">
	    @foreach($vehicles as $vehicle)	
		  <div class="col-sm-6 col-md-4">
		    <div class="thumbnail">
		      <img src="{!! $vehicle->photo !!}" alt="Vehicle Photo">
		      <div class="caption">
		        <h3>{!! $vehicle->vehiclemodel->modelmake->name !!} {!! $vehicle->vehiclemodel->name !!}</h3>	        
		        <p>Year : {!! $vehicle->year !!}</p>
		        <p>Plate No : {!! $vehicle->plate_no !!}</p>
		        <p>Capacity : {!! $vehicle->seating_capacity !!}</p>			        
		        <p>
		        	@if ($vehicle->available == 1)
		        		<a href="{!! URL::route('transpo.select.add',$vehicle->id) !!}" class="btn btn-primary" role="button"> <i class="fa fa-arrows-alt" aria-hidden="true"></i> Select</a> 
		        	@else
						<a href="#" class="btn btn-danger"> Select</a>
		        	@endif
				   <a href="{!! URL::route('transpo.vehicle.details', $vehicle->id) !!}" class="btn btn-info"> <i class="fa fa-list" aria-hidden="true"></i> Details</a>
		        </p>
		      </div>
		    </div>
		  </div>
		@endforeach
	  </div><!-- /.box-body -->
	</div><!-- /.box -->
	<!-- Approvers Dash -->
	<div class="box box-success">
	  <div class="box-header with-border">
	    <h3 class="box-title">For Approval</h3> <span data-toggle="tooltip" title="{!! count($needapprovals) !!} Needs Approval" class="badge bg-green">{!! count($needapprovals) !!}</span>
	    <div class="box-tools pull-right">
	      <!-- Buttons, labels, and many other things can be placed here! -->
	      <!-- Here is a label for example -->
	      <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
	    </div><!-- /.box-tools -->
	  </div><!-- /.box-header -->
	  <div class="box-body">
	  		<div class="table-responsive">
	    	@if (count($needapprovals) > 0)
				<table class="table table-bordered">
					<tr class="bg-orange">
						<th>SN</th>
						<th>Requested By</th>
						<th>Purpose</th>
						<th>Vehicle</th>
						<th>Model</th>
						<th>Start Date</th>
						<th>End Date</th>
						<th></th>
					</tr>
				@php
					$sn=1;
				@endphp
				@foreach($needapprovals as $needapproval)
					<tr>
						<td>{!! $sn++ !!}</td>
						<td>{!! $needapproval->booking->requester->fullname !!}</td>
						<td>{!! $needapproval->booking->purpose !!}</td>
						<td>{!! $needapproval->booking->vehicle->vehiclemodel->name !!}</td>
						<td>{!! $needapproval->booking->vehicle->year !!}</td>
						<td>{!! \Carbon\Carbon::parse($needapproval->booking->start_date)->format('M d, Y') !!}</td>
						<td>{!! \Carbon\Carbon::parse($needapproval->booking->end_date)->format('M d, Y') !!}</td>
						<td><a href="{!! URL::route('transpo.booking.approve',[$needapproval->id, $needapproval->transpo_booking_id]) !!}" class="btn btn-primary btn-xs"> <i class="fa fa-thumbs-o-up" aria-hidden="true"></i> Approve</a> <a href="{!! URL::route('transpo.booking.reject',[$needapproval->id, $needapproval->transpo_booking_id]) !!}" class="btn btn-danger btn-xs"> <i class="fa fa-thumbs-o-down" aria-hidden="true"></i> Reject</a></td>
					</tr>
				@endforeach
				</table>
			@endif
			</div>
	  </div><!-- /.box-body -->
	</div><!-- /.box -->
	<!-- For Release -->
	@if (($isHODAdmin == 1) || ($isADFA == 1))
	<div class="box box-solid box-info">
	  <div class="box-header with-border">
	    <h3 class="box-title"> <i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i> For Release</h3>
	    <div class="box-tools pull-right">
	      <!-- Buttons, labels, and many other things can be placed here! -->
	      <!-- Here is a label for example -->
	      
	    </div><!-- /.box-tools -->
	  </div><!-- /.box-header -->
	  <div class="box-body">
	    <div class="table-responsive">
	    	<table class="table table-hover">
	    		<thead>
	    		<tr>
	    			<th>SN</th>
					<th>Requested By</th>
					<th>Driver</th>
					<th>Vehicle</th>
					<th>Plate No</th>
					<th>Start Date</th>
					<th>End Date</th>
					<th></th>	    		
				</tr>
				</thead>
					@php
						$sn=1;
					@endphp
					<tbody>
					@foreach($forRelease as $release)
					<tr>
						<td>{!! $sn++ !!}</td>
						<td>{!! $release->requester->fullname !!}</td>
						<td>@if ($release->transpo_driver_id != 0) {!! $release->driver->fullname !!} @else <p class="text-danger"><strong>Need Driver</strong></p> @endif</td>
			    		<td>{!! $release->vehicle->vehiclemodel->name !!}</td>
			    		<td>{!! $release->vehicle->plate_no !!}</td>
			    		<td>{!! \Carbon\Carbon::parse($release->start_date)->format('M d, Y') !!}</td>
			    		<td>{!! \Carbon\Carbon::parse($release->end_date)->format('M d, Y') !!}</td>
			    		<td><a href="{!! URL::route('transpo.booking.release',$release->id) !!}" class="btn btn-xs btn-success"> <i class="fa fa-location-arrow" aria-hidden="true"></i> Release</a></td>
					</tr>
					@endforeach
				</tbody>
	    	</table>
	    </div>
	  </div><!-- /.box-body -->
	  <div class="box-footer">
	    List of Vehicles ready for release
	  </div><!-- box-footer -->
	</div><!-- /.box -->

	<div class="box box-solid box-danger">
	  <div class="box-header with-border">
	    <h3 class="box-title"><i class="fa fa-reply" aria-hidden="true"></i> To Be Returned Vehicles</h3>
	    <div class="box-tools pull-right">
	      <!-- Buttons, labels, and many other things can be placed here! -->
	      <!-- This will cause the box to collapse when clicked -->
			<button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
	      <!-- Here is a label for example -->	      
	    </div><!-- /.box-tools -->
	  </div><!-- /.box-header -->
	  <div class="box-body">
	    <table class="table table-striped">
	    	<tr>
	    		<th>SN</th>
	    		<th>Released Date/Time</th>
	    		<th>Requested By</th>
	    		<th>Assigned Driver</th>	    		
	    		<th>Vehicle</th>
	    		<th>Plate No</th>
	    		<th>Start Date</th>
	    		<th>End Date</th>
	    		<th></th>
	    	</tr>
	    	@php
				$sn=0;
	    	@endphp
	    	@foreach($forReturn as $return)
	    	<tr>
	    		<td>{!! ++$sn !!}</td>
	    		<td>{!! $return->release_date !!}</td>
	    		<td>{!! $return->requester->fullname !!}</td>
	    		<td>{!! $return->driver->fullname !!}</td>	    		
	    		<td>{!! $return->vehicle->vehiclemodel->name !!}</td>
	    		<td>{!! $return->vehicle->plate_no !!}</td>
	    		<td>{!! \Carbon\Carbon::parse($return->start_date)->format('d-M-Y') !!}</td>
	    		<td>{!! \Carbon\Carbon::parse($return->end_date)->format('d-M-Y') !!}</td>
	    		<td><a href="{!! URL::route('transpo.booking.return',$return->id) !!}" class="btn btn-xs btn-info"> <i class="fa fa-reply" aria-hidden="true"></i> Return</a></td>
	    	</tr>
	    	@endforeach
	    </table>
	  </div><!-- /.box-body -->
	  <div class="box-footer">
	    List of Vehicles to be returned
	  </div><!-- box-footer -->
	</div><!-- /.box -->
	@endif	
</section>
@endsection
