<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class JsonController extends Controller
{
    public function loadmodel($makeId) 
    {
    	$models = \DB::table('transpo_models')
    				->select('id','name as text')
    				->where('transpo_make_id',$makeId)
    				->get();

    	return Response()->json($models);
    }
}
