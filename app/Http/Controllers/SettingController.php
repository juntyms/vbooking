<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\TranspoMenu;
use App\Models\User;
use App\Models\TranspoMenuUser;

class SettingController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $menus = TranspoMenu::pluck('name_en','id');

        $users = User::where('resigned_at','0000-00-00 00:00:00')
                ->where('isStaff','1')
                ->select(\DB::raw("CONCAT(fullname,' - ',email) as completeName"),'id')
                ->pluck('completeName','id');

        $menuUsers = TranspoMenuUser::orderBy('user_id','ASC')->get();

        return view('transpo.setting.index')
                    ->with('nav',9)
                    ->with('menus',$menus)
                    ->with('users',$users)
                    ->with('menuUsers',$menuUsers);
    }

    public function addmenu(Request $request)
    {
        /** Query First if menu already assigned to user */
        $menuUser = TranspoMenuUser::where('user_id',$request->user_id)
                                ->where('transpo_menu_id',$request->transpo_menu_id)
                                ->get();
                    
        if ($menuUser->isEmpty()) { /** Check if menu already assigned to user */
            /** Add Assignment */
            TranspoMenuUser::create($request->all());

            \Flash::success('Menu Assignment Successful');
        } else {
            \Flash::error(' Unable to assign selected Menu Already Assigned');
        }

        return redirect()->route('transpo.setting.index')->withInput();
    }

    public function delmenu($id)
    {
        $menuUser = TranspoMenuUser::findOrFail($id);

        $menuUser->delete();

        \Flash::error('Menu Deleted');

        return redirect()->route('transpo.setting.index');
    }
}
