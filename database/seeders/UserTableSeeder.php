<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('users')->delete();
        User::create([
            'name'=> 'test user',
            'email' => 'test@test.com',
            'password' => bcrypt::Hash('password')
        ]);
    }
}
