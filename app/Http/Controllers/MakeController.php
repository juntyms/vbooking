<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\TranspoMake;

class MakeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
    	$makes = TranspoMake::all();

    	return view('transpo.make.index')
                ->with('makes',$makes)
                ->with('nav',2);
    }

    public function new()
    {
    	return view('transpo.make.new')
                ->with('nav',2);
    }

    public function save(Request $request)
    {
    	TranspoMake::create($request->all());

    	\Flash::success('Make Successfully saved');

    	return redirect()->route('transpo.make.index');
    }

    public function edit($id)
    {
    	$make = TranspoMake::findOrFail($id);

    	return view('transpo.make.edit')
                ->with('make',$make)
                ->with('nav',2);
    }

    public function update(Request $request, $id)
    {
    	$make = TranspoMake::findOrFail($id);

    	$make->update($request->all());

    	\Flash::success('Make Successfully Updated');

    	return redirect()->route('transpo.make.index');
    }
}
