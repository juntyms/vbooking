<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTranspoStatusIdFromTranspoBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transpo_bookings', function (Blueprint $table) {
            $table->unsignedBigInteger('transpo_status_id');
            $table->foreign('transpo_status_id')->references('id')->on('transpo_status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transpo_bookings', function (Blueprint $table) {
            $table->dropColumn('transpo_status_id');
            $table->dropColumn('transpo_status_id');
        });
    }
}
