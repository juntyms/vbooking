@extends('transpo.base')

@section('breadcrumbs')
<section class="content-header">
  <h1>
    Tranporation System
    <small>Approver Assignment</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Approver Assignment</a></li>        
  </ol>
</section>
@endsection

@section('maincontent')
<section class="content">
	<div class="box box-solid box-warning">
	  <div class="box-header with-border">
	    <h3 class="box-title">Approver Assignments</h3>
	    <div class="box-tools pull-right">
	      <!-- Buttons, labels, and many other things can be placed here! -->
	      <!-- Here is a label for example -->	      
	    </div><!-- /.box-tools -->
	  </div><!-- /.box-header -->
	  <div class="box-body">
	  	<a href="{!! URL::route('transpo.approving.add') !!}" class="btn btn-app"><i class="fa fa-plus-circle" aria-hidden="true"></i> Assign</a>


		<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
			@foreach($depts as $dept)	
		  <div class="panel panel-default">
		    <div class="panel-heading" role="tab" id="heading{!! $dept->id !!}">
		      <h4 class="panel-title">
		        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse{!! $dept->id !!}" aria-expanded="true" aria-controls="collapse{!! $dept->id !!}">
		          {!! $dept->name !!}
		        </a>
		      </h4>
		    </div>
		    <div id="collapse{!! $dept->id !!}" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading{!! $dept->id !!}">
		      <div class="panel-body">
		{{--      	@if (count($dept->transpoapprovers) > 0) --}}
				@if (count($approverUsers) >0 )
				<table class="table table-bordered">
						<tr class="bg-orange">
							<th>Role</th>
							<th>User</th>
							<th>Email</th>							
							<th></th>
						</tr>
					{{-- @foreach($dept->transpoapprovers as $approver) --}}
						@foreach($approverUsers as $approver)
						@if($approver->dept_id == $dept->id)
						<tr>
							<td>{!! $approver->role->name !!}</td>
							<td>{!! $approver->user->fullname !!}</td>
							<td>{!! $approver->user->email !!}</td>							
							<td><a href="{!! URL::route('transpo.approving.delete',$approver->id) !!}" class="text-danger"><i class="fa fa-window-close" aria-hidden="true"></i></a></td>
						</tr>
						@endif
						@endforeach
				</table>		        
				@endif
		      </div>
		    </div>
		  </div>
			@endforeach
		</div>
		
	  </div><!-- /.box-body -->
	  <div class="box-footer">
	    Approver Assignments
	  </div><!-- box-footer -->
	</div><!-- /.box -->
</section>
@endsection