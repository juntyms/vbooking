<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTranspoMaintenancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transpo_maintenances', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('transpo_vehicle_id');
            $table->foreign('transpo_vehicle_id')->references('id')->on('transpo_vehicles');
            $table->date('maintenance_date');
            $table->integer('km');
            $table->date('next_maintenance_date');
            $table->integer('next_km');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('transpo_maintenances');
    }
}
