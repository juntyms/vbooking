
<li @if($nav == 1) class="transpo-nav active" @else class="transpo-nav" @endif>
    <a href="{!! URL::route('transpo.index') !!}"><i class="ion ion-bag"></i> <span> Transpo Dashboard</span> </a>
</li>
@foreach($usermenus as $usermenu)
    @if ($usermenu->transpo_menu_id == 1)
    <li @if($nav == 2) class="transpo-nav active" @else class="transpo-nav" @endif>
        <a href="{!! URL::route('transpo.make.index') !!}"><i class="fa fa-bus" aria-hidden="true"></i> Make </span> </a>
    </li>
    @endif
    @if ($usermenu->transpo_menu_id == 2)
    <li @if($nav == 3) class="transpo-nav active" @else class="transpo-nav" @endif>
        <a href="{!! URL::route('transpo.vehicle.index') !!}"><i class="fa fa-car" aria-hidden="true"></i> Vehicle </span> </a>
    </li>
    @endif
    @if ($usermenu->transpo_menu_id == 3)
    <li @if($nav == 4) class="transpo-nav active" @else class="transpo-nav" @endif>
        <a href="{!! URL::route('transpo.driver.index') !!}"><i class="fa fa-address-card" aria-hidden="true"></i> <span> Drivers </span> </a>
    </li>
    @endif
    @if ($usermenu->transpo_menu_id == 4)
    <li @if($nav == 5) class="transpo-nav active" @else class="transpo-nav" @endif>
        <a href="{!! URL::route('transpo.approving.index') !!}"><i class="fa fa-check-square-o" aria-hidden="true"></i> <span> Approvers Assignment </span> </a>
    </li>
    @endif
    @if ($usermenu->transpo_menu_id == 5)
    <li @if($nav == 6) class="transpo-nav active" @else class="transpo-nav" @endif>
        <a href="{!! URL::route('transpo.approver.index') !!}"><i class="fa fa-check-circle" aria-hidden="true"></i> <span> Approver Description </span> </a>
    </li>
    @endif
    @if ($usermenu->transpo_menu_id == 6)
    <li @if($nav == 7) class="transpo-nav active" @else class="transpo-nav" @endif>
        <a href="{!! URL::route('transpo.approver.seq.index') !!}"><i class="fa fa-check" aria-hidden="true"></i> <span> Approver Seq </span> </a>
    </li>
    @endif
    @if ($usermenu->transpo_menu_id == 7)
    <li @if($nav == 8) class="transpo-nav active" @else class="transpo-nav" @endif>
        <a href="{!! URL::route('transpo.report.index') !!}"><i class="fa fa-newspaper-o" aria-hidden="true"></i> <span> Report </span> </a>
    </li>
    @endif
    @if ($usermenu->transpo_menu_id == 8)
    <li @if($nav == 9) class="transpo-nav active" @else class="transpo-nav" @endif>
        <a href="{!! URL::route('transpo.setting.index') !!}"><i class="fa fa-cogs" aria-hidden="true"></i> <span> Setting </span> </a>
    </li>
    @endif

@endforeach
