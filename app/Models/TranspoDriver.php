<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TranspoDriver extends Model
{
    protected $table = 'transpo_drivers';

    protected $fillable = ['user_id','status'];

    public function driver()
    {
    	return $this->hasOne('App\User','id','user_id');
    }
}
