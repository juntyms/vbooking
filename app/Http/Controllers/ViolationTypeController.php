<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\TranspoTypeOfViolations;
use Auth;

class ViolationTypeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
    	$type_of_violations = TranspoTypeOfViolations::all();    	

    	return view('transpo.violation.index')
    				->with('type_of_violations',$type_of_violations)
    				->with('nav',3);
    }

    public function save(Request $request)
    {
    	TranspoTypeOfViolations::create($request->all());

        \Flash::success('Violation Type Successfully Added');
        
    	return redirect()->route('transpo.violationtype.index');
    }

    public function edit()
    {

    }

    public function update()
    {

    }
}
