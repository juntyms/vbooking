<?php

namespace App\Http\Composers;

use Illuminate\Contracts\View\View;
use Auth;
use App\User;
use App\StorePrivilege;

class NavigationComposer {

		public function compose(View $view)
		{

	        $permission = \DB::table('app_privileges_user')
	            ->where('user_id',Auth::user()->id)
	            ->first();

	        $isPermission = 0;

	        if ($permission) {
	          $isPermission = $permission->app_privilege_id;
	        }

	        $applists = User::findOrFail(Auth::user()->id);

	        $filePhoto = $applists->photo_path;

	        $view->with('isPermission',$isPermission)
	            ->with('filePhoto',$filePhoto)
	            ->with('applists',$applists);

		}

		public function loadDept(View $view)
		{
			//Check if Coordinator
	            $userType = \DB::table('app_privileges_user')
	                            ->where('user_id','=', Auth::user()->id)
	                            ->select('app_privilege_id')
	                            ->first();

	            if (($userType->app_privilege_id == 1) || ($userType->app_privilege_id == 2))  {
	                //Admin and QA Unit
	                $departments = \DB::table('depts')->pluck('name','id');
	            }
	            if ($userType->app_privilege_id == 3) {
	                //qa Coordinator
	                $departments = \DB::table('depts')
	                                    ->where('id',Auth::user()->dept_id)
	                                    ->pluck('name','id');
	            }

	            $view->with('departments',$departments);

		}

		public function storePrivilege(view $view)
		{


				$storePriv = \DB::table('store_privilege_user')
												->where('user_id', Auth::user()->id)
												->first();


				//dd($storePriv);
				if (!$storePriv) {
						$storeAccess = 0;
				} else {
						$storeAccess = $storePriv->store_privilege_id;
				}

				$view->with('storeAccess',$storeAccess);
		}

		public function hrPrivilege(view $view)
		{
			$hrPriv = \DB::table('hr_privilege_user')
									->where('user_id', Auth::user()->id)
									->first();

			if (!$hrPriv) {
				$hrAccess = 0;
			} else {
				$hrAccess = $hrPriv->hr_privilege_id;
			}

			$view->with('hrAccess',$hrAccess);
		}

		public function hrMenu(view $view)
		{
			$hrMenus = \DB::table('hr_menu_user')
							->where('user_id',Auth::user()->id)
							->get();

			$view->with('hrMenus',$hrMenus);
		}

		public function bsPrivilege(view $view)
		{
			$bsPriv = \DB::table('bs_user_privilege')
							->where('user_id', Auth::user()->id)
							->first();

			$bsAccess = $bsPriv->bs_privilege_id;

			$view->with('bsAccess', $bsAccess);
		}
}
