@extends('transpo.base')

@section('breadcrumbs')
<section class="content-header">
  <h1>
    Tranporation System
    <small>Dashboard Panel</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Report</a></li>
    <li><a href="#">Booking Requests</a></li>
  </ol>
</section>
@endsection

@section('maincontent')
<section class="content">
    <div class="box box-warning">
    <div class="box-header with-border">
        <h3 class="box-title">Booking Request Report</h3>
        <div class="box-tools pull-right">
        <!-- Buttons, labels, and many other things can be placed here! -->
        <!-- Here is a label for example -->
        </div>
        <!-- /.box-tools -->
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <table class="table table-bordered">
            <tr>
                <th>Date Requested</th>
                <th>Vehicle Name</th>
                <th>Plate No</th>
                <th>Start Date</th>
                <th>End Date</th>
                <th>Requested By</th>
                <th>Requester Email</th>
                <th>Status</th>
            </tr>
            @foreach($bookingRequests as $booking)
            <tr>
                <td>{!! \Carbon\Carbon::parse($booking->created_at)->format('d M Y') !!}</td>
                <td>{!! $booking->vehicle->vehiclemodel->name !!}</td>
                <td>{!! $booking->vehicle->plate_no !!}</td>
                <td>{!! \Carbon\Carbon::parse($booking->start_date)->format('d M Y') !!}</td>
                <td>{!! \Carbon\Carbon::parse($booking->end_date)->format('d M Y') !!}</td>
                <td>{!! $booking->requester->fullname !!}</td>
                <td>{!! $booking->requester->email !!}</td>
                <td>{!! $booking->status->name !!}</td>
            </tr>
            @endforeach
        </table>
    </div>
    <!-- /.box-body -->
    <div class="box-footer">
        Booking Request List
    </div>
    <!-- box-footer -->
    </div>
    <!-- /.box -->    
    
</section>
@endsection