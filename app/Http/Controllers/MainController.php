<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Auth;
use App\Models\User;
use Adldap\Adldap;
use Carbon\Carbon;
use Storage;
use File;
use App\Models\MainApplist;

use Illuminate\Database\QueryException;
use Exception;

class MainController extends Controller
{
	public function index()
	{
		return redirect()->route('user.dashboard');
	}

    public function login()
    {
    	return view('login');
    }


    public function postLogin(Request $request)
    {
        $credentials = $request->validate([
            'email' => ['required', 'email'],
            'password' => ['required'],
        ]);

        if (Auth::attempt($credentials)) {
            $request->session()->regenerate();

            return redirect()->route('transpo.index');
        }

        return back()->withErrors([
            'email' => 'The provided credentials do not match our records.',
        ]);

       
    }

    public function dashboard()
    {
    	//$applists = User::findOrFail(Auth::user()->id)->applists()->where('isGuest',0)->get();
        
		//$guestApplists = MainApplist::where('isGuest',1)->get(); //All

        //$staffApplists = MainApplist::where('isGuest',2)->get(); //staff Only

        //$studentApplists = MainApplist::where('isGuest',3)->get(); //Student Only

			//dd($applists);

    	//$filePhoto = $applists->photo_path;
			/*
			$user = \DB::table('users')->where('isStudent',1)->where('id',Auth::user()->id)->first();
			$isStudent = 0;
			if ($user) {
				$isStudent = 1;
			} */

    	return view('main.dashboard')
    				//->with('applists',$applists)
					->with('guestApplists',$guestApplists)
                    ->with('staffApplists',$staffApplists)
                    ->with('studentApplists', $studentApplists);
    				//->with('filePhoto', $filePhoto);
    }

    public function loadimage($folder, $file)
    {

		$path =  $folder . '/' . $file;

    	if(!\Storage::exists($path)) abort(404);

	    $file = \Storage::get($path);
	    $type = \Storage::mimeType($path);

	    $response = \Response::make($file, 200);
	    $response->header("Content-Type", $type);

	    return $response;
    }

    public function logout()
    {
    	Auth::logout();

    	return redirect()->route('transpo.index');
    }

    public function profile()
    {
    	$user = User::findOrFail(Auth::user()->id);

    	//$filePhoto = $user->photo_path;

    	return view('user.profile')
    		->with('user',$user);
    		//		->with('filePhoto', $filePhoto);
    }

    public function updateprofile(Request $request, $id)
    {
    	$user = User::findOrFail($id);

    	$file = $request->file('avatar');
        if ($file) {
        	$extension = $file->getClientOriginalExtension();

        	$filename = $file->getFilename(). '.'.$extension;

        	if ($request->file('avatar')->isValid()) {

        		// Check if file exist
        		$exists = Storage::disk('local')->exists('staff/'. $id . '.'.$extension);

        		//if file exist delete the old file

        		if ($exists) {
        			Storage::delete('staff/'. $id . '.'.$extension);
        		}

        		Storage::disk('local')->put('staff/'. $filename, File::get($file));

        		Storage::move('staff/'.$filename, 'staff/'. $id . '.'.$extension);

        		$user->photo_path = $id . '.'.$extension;
        		$user->save();
            \Flash::success('Avatar successfully updated');
        		return redirect()->route('user.dashboard');
    			}
        }

				\Flash::error('Avatar not updated, no File Selected');

		return redirect()->route('user.dashboard');

    }

    public function settings()
    {
        if (Auth::user()->isAdmin == 1)
        {
            $users = User::where('isStaff',1)
                    ->orderBy('username','asc')
                    ->pluck('username','id');

            // change this in the future to display only active applications
            // add column isActive to mainapplist table
            $applists = MainApplist::all()->pluck('appName','id');

            $applications = MainApplist::with('users')->get();

            return view('user.settings',
                        ['users'=>$users,
                         'applists'=>$applists,
                         'applications'=>$applications]);
        }

        \Flash::error('You Have no permission to access that resource');

        return redirect()->route('user.dashboard');
    }

    public function addApp(Request $request)
    {
        $user = User::findOrFail($request->user_id);
        try {
            $user->applists()->attach($request->main_applist_id);
            \Flash::success('Application Access Added for that user');
        } catch(QueryException $e) {
            if (preg_match('/Duplicate entry/',$e->getMessage())){
            /*
            return response([
                'success' => false,
                'message' => 'Role exists for that user'
            ], 500);*/
            \Flash::error('Access Exists for that user');
           }
        }

        return redirect()->route('settings');
    }

    public function removeApp($appId, $userId)
    {
        if (Auth::user()->isAdmin == 1)
        {
            $user = User::findOrFail($userId);
            $user->applists()->detach($appId);
        }

        return redirect()->route('settings');
    }
}
