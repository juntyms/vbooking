<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TranspoBooking extends Model
{
    protected $table = 'transpo_bookings';

    protected $fillable = ['transpo_vehicle_id','requested_by','transpo_driver_id','start_date','end_date','purpose','transpo_status_id'];

    public function requester()
    {
    	return $this->hasOne('App\User','id','requested_by');
    }

    public function vehicle()
    {
    	return $this->hasOne('App\TranspoVehicle','id','transpo_vehicle_id');
    }

    public function status()
    {
    	return $this->hasOne('App\TranspoStatus','id','transpo_status_id');
    }

    public function approver()
    {
    	return $this->hasMany('App\TranspoBookingApproval','transpo_booking_id','id');
    }

    public function driver()
    {
        return $this->hasOne('App\User','id','transpo_driver_id');
    }

    public function released_by()
    {
        return $this->hasOne('App\User','id','release_by');
    }

    public function received_by()
    {
        return $this->hasOne('App\User','id','accepted_by');
    }
}
