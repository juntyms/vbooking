<?php

namespace App\Http\Composers;

use Illuminate\Contracts\View\View;

class HrComposer {

  public function employeeForm(View $view)
  {
      $nationalities = \DB::table('nationalities')->pluck('nationality','id');

      $positions = \DB::table('positions')->pluck('position','id');

      $departments = \DB::table('depts')->pluck('name','id');

      $employers = \DB::table('employers')->pluck('employers','id');

      $highestqualifications = \DB::table('hr_qualifications')->pluck('name','id');

      $deptsections = \DB::table('deptsections')->pluck('completename','id');

      $titles = \DB::table('titles')->pluck('name','id');

      $status = \DB::table('hr_employee_status')->pluck('status','id');

      $view->with('nationalities',$nationalities)
          ->with('positions',$positions)
          ->with('departments',$departments)
          ->with('employers',$employers)
          ->with('highestqualifications',$highestqualifications)
          ->with('deptsections', $deptsections)
          ->with('titles',$titles)
          ->with('status',$status);
  }

}
