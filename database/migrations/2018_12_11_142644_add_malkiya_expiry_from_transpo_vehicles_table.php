<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMalkiyaExpiryFromTranspoVehiclesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transpo_vehicles', function (Blueprint $table) {
            $table->timestamp('malkiya_expiry')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transpo_vehicles', function (Blueprint $table) {
            $table->dropColumn('malkiya_expiry');
        });
    }
}
