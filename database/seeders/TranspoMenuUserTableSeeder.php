<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class TranspoMenuUserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('transpo_menu_user')->delete();
        TranspoMenuUser::create(['transpo_menu_id'=>'1','user_id'=>'1']);
        TranspoMenuUser::create(['transpo_menu_id'=>'2','user_id'=>'1']);
        TranspoMenuUser::create(['transpo_menu_id'=>'3','user_id'=>'1']);
        TranspoMenuUser::create(['transpo_menu_id'=>'4','user_id'=>'1']);
        TranspoMenuUser::create(['transpo_menu_id'=>'5','user_id'=>'1']);
        TranspoMenuUser::create(['transpo_menu_id'=>'6','user_id'=>'1']);
        TranspoMenuUser::create(['transpo_menu_id'=>'7','user_id'=>'1']);
    }
}
