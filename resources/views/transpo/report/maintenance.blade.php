@extends('transpo.base')

@section('breadcrumbs')
<section class="content-header">
  <h1>
    Tranporation System
    <small>Dashboard Panel</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Report</a></li>
    <li><a href="#">Vehicle Maintenance</a></li>
  </ol>
</section>
@endsection

@section('maincontent')
<section class="content">
    <div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Vehicle Maintenance Report</h3>
        <div class="box-tools pull-right">
        <!-- Buttons, labels, and many other things can be placed here! -->
        <!-- Here is a label for example -->
        </div>
        <!-- /.box-tools -->
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <table class="table table-bordered">
            <tr class="bg-primary">
                <th>Date Updated</th>
                <th>Vehicle Name</th>
                <th>Plate No</th>
                <th>Maintenance Date</th>
                <th>Maintenance Kilometer</th>
                <th>Next Maintenance Date</th>
                <th>Next Maintenance Kilometer</th>
                <th>Updated By</th>
                <th>Email</th>
            </tr>
            @foreach($maintenances as $maintenance)
            <tr>
                <td>{!! \Carbon\Carbon::parse($maintenance->created_at)->format('d M Y') !!}</td>
                <td>{!! $maintenance->vehicle->vehiclemodel->name !!}</td>
                <td>{!! $maintenance->vehicle->plate_no !!}</td>
                <td>{!! \Carbon\Carbon::parse($maintenance->maintenance_date)->format('d M Y') !!}</td>
                <td>{!! $maintenance->km !!}</td>
                <td>{!! \Carbon\Carbon::parse($maintenance->next_maintenance_date)->format('d M Y') !!}</td>
                <td>{!! $maintenance->next_km !!}</td>
                <td>{!! $maintenance->updatedBy->fullname !!}</td>
                <td>{!! $maintenance->updatedBy->email !!}</td>                
            </tr>
            @endforeach
        </table>
    </div>
    <!-- /.box-body -->
    <div class="box-footer">
        Maintenance List
    </div>
    <!-- box-footer -->
    </div>
    <!-- /.box -->    
    
</section>
@endsection