<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TranspoAccidentHistory extends Model
{
    protected $table = 'transpo_accident_history';

    protected $fillable = ['accident_date','driver_id','description','updated_by','transpo_vehicle_id'];

    public function driver()
    {
    	return $this->hasOne('App\User','id','driver_id');
    }

    public function vehicle()
    {
        return $this->hasOne('App\TranspoVehicle','id','transpo_vehicle_id');
    }

    public function updatedBy()
    {
        return $this->hasOne('App\User','id','updated_by');
    }

}
