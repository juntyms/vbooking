@extends('transpo.base')

@section('breadcrumbs')
<section class="content-header">
  <h1>
    Tranporation System
    <small>Vehicle Reject</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Booking</a></li>        
    <li><a href="#">Vehicle</a></li>        
    <li><a href="#">Reject</a></li>        
  </ol>
</section>
@endsection

@section('maincontent')
	<section class="content">
    <div class="row">
      <div class="col-md-3">
        <img src="{!! asset($booking->vehicle->photo) !!}" alt="">
      </div>
      <div class="col-md-4">
        <table class="table table-bordered">        
          <tr>
            <td class="bg-warning">Plate No</td>
            <td>{!! $booking->vehicle->plate_no !!}</td>
            <td class="bg-warning">Seating Capacity</td>
            <td>{!! $booking->vehicle->seating_capacity !!}</td>
          </tr>
          <tr>
            <td class="bg-warning">Model</td>
            <td>{!! $booking->vehicle->vehiclemodel->name !!}</td>
            <td class="bg-warning">Year</td>
            <td>{!! $booking->vehicle->year !!}</td>
          </tr>
          <tr>          
          </tr>
          <tr>
            <td class="bg-warning">Requested by</td>
            <td>{!! $booking->requester->fullname !!}</td>
            <td class="bg-warning">Start Date</td>
            <td>{!! \Carbon\Carbon::parse($booking->start_date)->format('d-M-Y') !!}</td>
          </tr>
          <tr>
            <td class="bg-warning">Purpose</td>
            <td>{!! $booking->purpose !!}</td>
            <td class="bg-warning">End Date</td>
            <td>{!! \Carbon\Carbon::parse($booking->end_date)->format('d-M-Y') !!}</td>
          </tr>
        </table>
      </div>
    </div>
    <div class="row">
      <div class="col-md-7">
      {!! Form::open(['route'=>['transpo.booking.postreject',$id, $booking->id]]) !!}
        {!! Form::textarea('reject_reason',null, ['class'=>'form-control']) !!}
        <button class="btn btn-xs btn-danger"><i class="fa fa-thumbs-o-down" aria-hidden="true"></i> Reject</button>
      {!! Form::close() !!}
      </div>  
    </div>
    
  </section>
@endsection