<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TranspoApproverSeq extends Model
{
    protected $table = 'transpo_approver_seqs';

    protected $fillable = ['transpo_approver_id','seq','dept_id'];

    public function role()
    {
    	return $this->belongsTo('App\TranspoApprover','transpo_approver_id','id');
    }

}
