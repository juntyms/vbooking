<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFineFromTranspoTypeOfViolationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transpo_type_of_violations', function (Blueprint $table) {
            $table->integer('fine')->nullable();
            $table->integer('black_points')->nullable();
            $table->integer('updated_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transpo_type_of_violations', function (Blueprint $table) {
            $table->dropColumn('fine');
            $table->dropColumn('black_points');
            $table->dropColumn('updated_by');
        });
    }
}
