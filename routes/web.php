<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TransportationController;
use App\Http\Controllers\MainController;
use App\Http\Controllers\ViolationTypeController;
use App\Http\Controllers\VehicleController;
use App\Http\Controllers\MakeController;
use App\Http\Controllers\ModelController;
use App\Http\Controllers\JsonController;
use App\Http\Controllers\DriverController;
use App\Http\Controllers\ApproverController;
use App\Http\Controllers\ApprovingController;
use App\Http\Controllers\ApproverseqController;
use App\Http\Controllers\SelectController;
use App\Http\Controllers\BookingController;
use App\Http\Controllers\ReportController;
use App\Http\Controllers\SettingController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


/**********************************************************/
/************ Transportation system Route ***********************/

Route::get('/', [TransportationController::class, 'index'])->name('transpo.index');

Route::get('user/login', [MainController::class,'login'])->name('login');
Route::post('user/login', [MainController::class,'postLogin'])->name('postlogin');
Route::get('user/logout',[MainController::class,'logout'])->name('user.logout')->middleware('auth');

Route::get('transpo/selectvehicle', [TransportationController::class,'selectvehicle'])->name('transpo.selectvehicle');
Route::post('transpo/request', [TransportationController::class,'request'])->name('transpo.request');
Route::get('transpo/requestdetail/{id}', [TransportationController::class,'requestdetail'])->name('transpo.requestdetail');

Route::get('transpo/violationtype', [ViolationTypeController::class,'index'])->name('transpo.violationtype.index');
Route::post('transpo/violationtype/save', [ViolationTypeController::class,'save'])->name('transpo.violationtype.save');
Route::get('transpo/violationtype/{id}/edit', [ViolationTypeController::class,'edit'])->name('transpo.violationtype.edit');
Route::post('transpo/violationtype/{id}/update', [ViolationTypeController::class,'update'])->name('transpo.violationtype.update');

Route::get('transpo/vehicle', [VehicleController::class,'index'])->name('transpo.vehicle.index');
Route::get('transpo/vehicle/add', [VehicleController::class,'add'])->name('transpo.vehicle.add');
Route::post('tranpo/vehicle/add', [VehicleController::class,'save'])->name('transpo.vehicle.save');
Route::get('tranpo/vehicle/{id}/edit', [VehicleController::class,'edit'])->name('transpo.vehicle.edit');
Route::post('tranpo/vehicle/{id}/edit', [VehicleController::class,'update'])->name('transpo.vehicle.update');
Route::get('transpo/vehicle/{id}/details', [VehicleController::class,'details'])->name('transpo.vehicle.details');
//Route::post('transpo/vehicle/{id}/details', ['as'=>'transpo.vehicle.postdetails', 'uses'=>'tranpo\VehicleController@postdetails']);

Route::get('transpo/vehicle/{id}/maintenance', [VehicleController::class,'maintenance'])->name('transpo.vehicle.maintenance');
Route::post('transpo/vehicle/{id}/maintenance', [VehicleController::class,'postmaintenance'])->name('transpo.vehicle.postmaintenance');
Route::get('transpo/vehicle/{id}/trafficviolation', [ViolationController::class,'trafficviolation'])->name('transpo.vehicle.trafficviolation');
Route::post('transpo/vehicle/{id}/trafficviolation', [ViolationController::class,'posttrafficviolation'])->name('transpo.vehicle.posttrafficviolation');
Route::get('transpo/vehicle/{id}/accident', [VehicleController::class,'accident'])->name('transpo.vehicle.accident');
Route::post('transpo/vehicle/{id}/accident', [VehicleController::class,'postaccident'])->name('transpo.vehicle.postaccident');

Route::get('transpo/makes',[MakeController::class,'index'])->name('transpo.make.index');
Route::get('transpo/make/new', [MakeController::class,'new'])->name('transpo.make.new');
Route::post('transpo/make/new', [MakeController::class,'save'])->name('transpo.make.save');
Route::get('transpo/make/{id}/edit', [MakeController::class,'edit'])->name('transpo.make.edit');
Route::post('transpo/make/{id}/edit', [MakeController::class,'update'])->name('transpo.make.update');

Route::get('transpo/models/{makeId}',[ModelController::class,'index'])->name('transpo.model.index');
Route::get('transpo/model/new/{makeId}', [ModelController::class,'new'])->name('transpo.model.new');
Route::post('transpo/model/save', [ModelController::class,'save'])->name('transpo.model.save');
Route::get('transpo/model/{id}/edit', [ModelController::class,'edit'])->name('transpo.model.edit');
Route::post('transpo/model/{id}/edit', [ModelController::class,'update'])->name('transpo.model.update');
Route::get('transpo/model/{id}/delete', [ModelController::class,'delete'])->name('transpo.model.delete');

Route::get('transpo/json/make/{id}/model', [JsonController::class,'loadmodel'])->name('transpo.json.loadmodel');

Route::get('transpo/drivers',[DriverController::class,'index'])->name('transpo.driver.index');
Route::get('transpo/driver/add', [DriverController::class,'add'])->name('transpo.driver.add');
Route::post('tranpo/driver/{id}/add',[DriverController::class,'save'])->name('transpo.driver.save');
Route::get('transpo/driver/{id}/remove',[DriverController::class,'remove'])->name('transpo.driver.remove');

Route::get('transpo/approvers',[ApproverController::class,'index'])->name('transpo.approver.index');
Route::get('transpo/approver/add', [ApproverController::class,'add'])->name('transpo.approver.add');
Route::post('transpo/approver/add', [ApproverController::class,'save'])->name('transpo.approver.save');
Route::get('transpo/approver/{id}/edit',[ApproverController::class,'edit'])->name('transpo.approver.edit');
Route::post('transpo/approver/{id}/edit',[ApproverController::class,'update'])->name('transpo.approver.update');

Route::get('transpo/approvings', [ApprovingController::class,'index'])->name('transpo.approving.index');
Route::get('transpo/approving/add', [ApprovingController::class,'add'])->name('transpo.approving.add');
Route::post('transpo/approving/add', [ApprovingController::class,'save'])->name('transpo.approving.save');
Route::get('transpo/approving/{id}/delete', [ApprovingController::class,'delete'])->name('transpo.approving.delete');

Route::get('transpo/approver/seqs', [ApproverseqController::class,'index'])->name('transpo.approver.seq.index');
Route::get('transpo/approver/seq/add', [ApproverseqController::class,'add'])->name('transpo.approver.seq.add');
Route::post('tranpo/approver/seq/add', [ApproverseqController::class,'save'])->name('transpo.approver.seq.save');
Route::get('transpo/approver/seq/{id}/down', [ApproverseqController::class,'down'])->name('transpo.approver.seq.down');
Route::get('transpo/approver/seq/{id}/up', [ApproverseqController::class,'up'])->name('transpo.approver.seq.up');
Route::get('transpo/approver/seq/{id}/delete', [ApproverseqController::class,'delete'])->name('transpo.approver.seq.delete');

Route::get('transpo/select/{id}',[SelectController::class,'add'])->name('transpo.select.add');
Route::post('transpo/select/{id}', [SelectController::class,'reserve'])->name('transpo.select.reserve');

Route::get('transpo/booking/{bookingid}/release',[BookingController::class,'release'])->name('transpo.booking.release');
Route::post('transpo/booking/{bookingid}/release', [BookingController::class,'postrelease'])->name('tranpo.booking.postrelease');
Route::get('transpo/booking/{bookingid}/return', [BookingController::class,'return'])->name('transpo.booking.return');
Route::post('transpo/booking/{bookingid}/return', [BookingController::class,'postreturn'])->name('transpo.booking.postreturn');
Route::get('transpo/booking/{id}/{bookingid}/approve', [BookingController::class,'approve'])->name('transpo.booking.approve');
Route::get('transpo/booking/{id}/{bookingid}/reject', [BookingController::class,'reject'])->name('transpo.booking.reject');
Route::post('transpo/booking/{id}/{bookingid}/reject', [BookingController::class,'postreject'])->name('transpo.booking.postreject');

Route::get('transpo/reports', [ReportController::class,'index'])->name('transpo.report.index');
Route::post('transpo/report/request',[ReportController::class,'postrequest'])->name('transpo.report.postrequest');

Route::get('transpo/settings', [SettingController::class,'index'])->name('transpo.setting.index');
Route::post('transpo/setting/addmenu', [SettingController::class,'addmenu'])->name('transpo.setting.addmenu');
Route::post('transpo/setting/{id}/delmenu', [SettingController::class,'delmenu'])->name('transpo.setting.delmenu');
