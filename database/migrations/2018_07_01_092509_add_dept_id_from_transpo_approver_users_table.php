<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDeptIdFromTranspoApproverUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transpo_approver_users', function (Blueprint $table) {
            $table->integer('dept_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transpo_approver_users', function (Blueprint $table) {
            $table->dropColumn('dept_id');
        });
    }
}
