<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Vehicle Booking</title>

	<link rel="stylesheet" href="{{ asset('css/bootstrap/css/bootstrap.min.css') }}">
	

	<!-- Fonts Awesome -->
	<link rel="stylesheet" href="{{ asset('css/font-awesome-4.4.0/css/font-awesome.min.css') }}">
	

	<!-- Ionicons -->
	
	<link rel="stylesheet" href="{{ asset('css/lte/ionicons-2.0.1/css/ionicons.min.css')}}">

	<!-- Theme style -->
  	<link rel="stylesheet" href="{{ asset('css/lte/dist/css/AdminLTE.min.css')}}">

  <!-- AdminLTE Skins. Choose a skin from the css/skins
   folder instead of downloading all of them to reduce the load. -->
   <link rel="stylesheet" href="{{ asset('css/lte/dist/css/skins/_all-skins.min.css')}}">
	

  	<!-- iCheck -->
	<link rel="stylesheet" href="{{ asset('css/lte/plugins/iCheck/square/blue.css') }}">
  		
	<link rel="stylesheet" href="{{ asset('plugin/select/css/select2.min.css') }}">
	
	<link rel="stylesheet" href="{{ asset('css/lte/plugins/datatables/dataTables.bootstrap.css') }}">
	
	<link rel="stylesheet" href="{{ asset('css/lte/plugins/datepicker/datepicker3.css') }}">
	
	<link rel="stylesheet" href="{{ asset('css/lte/plugins/timepicker/bootstrap-timepicker.css') }}">
	
	<link rel="stylesheet" href="{{ asset('css/lte/plugins/daterangepicker/daterangepicker.css') }}">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9] -->
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <!-- [endif] -->

</head>
<body @yield('bodyclass')>

	<div class="wrapper">
		@include('main._mainheader')
		<!-- Left side column. contains the logo and sidebar -->
		@include('main._mainsidebar')

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
		<!-- Content Header (Page header) -->
		{{-- @include('main._breadcrumb') --}}
		@yield('breadcrumbs')

		{{-- @include('flash::message') --}}
		@if (session()->has('flash_notification.message'))
		    <div class="alert alert-{{ session('flash_notification.level') }}">
		        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>

		        {!! session('flash_notification.message') !!}
		    </div>
		@endif

		@if (count($errors) > 0)
		<div class="alert alert-danger">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		    <strong>Whoops!</strong> There were some problems with your input.<br><br>
		    <ul>
		        @foreach ($errors->all() as $error)
		            <li>{{ $error }}</li>
		        @endforeach
		    </ul>
		</div>
		@endif

		@yield('content')
		 </div>
  		<!-- /.content-wrapper -->
		@include('main._footer')

		  <!-- Control Sidebar -->
		  {{-- @include('main._rightsidebar') --}}
		  <!-- /.control-sidebar -->

	</div>
		<script src="{{ asset('css/bootstrap/js/jquery-2.1.3.min.js') }}"></script>
		
		<script src="{{ asset('css/bootstrap/js/bootstrap.min.js') }}"></script>
		

		<!-- iCheck -->		
		<script src="{{ asset('css/lte/plugins/iCheck/icheck.min.js') }}"></script>

		<script src="{{ asset('css/lte/dist/js/app.min.js') }}"></script>		

	<!--	<script src="{{ asset('css/lte/dist/js/pages/dashboard.js') }}"></script> -->
		
	<!--	<script src="{{ asset('css/lte/dist/js/demo.js') }}"></script> -->

		<script src="{{ asset('css/lte/plugins/datatables/jquery.dataTables.min.js') }}"></script>

		<script src="{{ asset('css/lte/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
		
		<script src="{{ asset('css/lte/plugins/datepicker/bootstrap-datepicker.js') }}"></script>

		<script src="{{ asset('css/lte/plugins/timepicker/bootstrap-timepicker.min.js') }}"></script>

		<script src="{{ asset('css/lte/plugins/daterangepicker/moment.min.js') }}"></script>

		<script src="{{ asset('css/lte/plugins/daterangepicker/daterangepicker.js') }}"></script>
		

		

		<script>
    	$('#flash-overlay-modal').modal();
		</script>


		@yield('jscript')

</body>
</html>
