@extends('mainlayout')

@section('bodyclass','class="hold-transition skin-yellow sidebar-mini"')

@section('subsidenav')
    @include('transpo.nav')
@endsection

@section('content')
  @yield('maincontent')  
@endsection