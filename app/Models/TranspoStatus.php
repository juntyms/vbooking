<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TranspoStatus extends Model
{
    protected $table = 'transpo_status';
}
