<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modles\TranspoVehicle;
use Auth;
use App\Modles\TranspoBooking;
use App\Modles\TranspoApproverUser;
use App\Modles\TranspoApproverSeq;
use App\Mail\TranspoNeedApproval;
use Illuminate\Support\Facades\Mail;


class SelectController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function add($id)
    {
    	$vehicle = TranspoVehicle::findOrFail($id);          

    	return view('transpo.select.reserve')
    				->with('vehicle',$vehicle)                    
    				->with('nav',1);
    }

    public function reserve(Request $request, $id)
    {
       // dd($request->all());
        //Check driver status
        $isDriverId = 0;
        if ($request->transpo_driver_id == 0) { // the driver is the requester
            $isDriverId = Auth::user()->id;
        } 

    	// Check if reserve is already reserved on the date

    	$vehicle = TranspoBooking::where('transpo_vehicle_id',$id)
    								->where('end_date','>=', date($request->start_date) .' 00:00:00')
    								->get();

             
        if ($vehicle->isEmpty()) {

            // Check Approvers                       
            $approvers = TranspoApproverSeq::where('dept_id',Auth::user()->dept_id)->orderBy('seq','asc')->get();

            //dd($approvers);

            //Check if approvers are set
            if ($approvers->count()) {
                // Get the InsertId of the added booking
                $bookingId = \DB::table('transpo_bookings')->insertGetId([
                                    'transpo_vehicle_id'=> $id,
                                    'requested_by'=> Auth::user()->id,
                                    'transpo_driver_id'=>$isDriverId,
                                    'start_date'=>\Carbon\Carbon::parse($request->start_date),
                                    'end_date'=>\Carbon\Carbon::parse($request->end_date),
                                    'purpose'=>$request->purpose,
                                    'created_at'=>\Carbon\Carbon::now(),
                                    'transpo_status_id'=>'1'
                                ]);

                foreach($approvers as $approver) {
                    // Set the steps for approval
                    if ($approver->seq == 1) { 
                    \DB::table('transpo_booking_approvals')->insert([
                                        'transpo_booking_id'=>$bookingId,
                                        'transpo_approver_id'=>$approver->transpo_approver_id,
                                        'approver_id'=> -1,
                                        'transpo_status_id'=>1,
                                        'created_at'=>\Carbon\Carbon::now()
                                    ]);
                    } else {
                    \DB::table('transpo_booking_approvals')->insert([
                                        'transpo_booking_id'=>$bookingId,
                                        'transpo_approver_id'=>$approver->transpo_approver_id,
                                        'approver_id'=> 0,
                                        'transpo_status_id'=>1,
                                        'created_at'=>\Carbon\Carbon::now()
                                    ]);
                    }
                }                
                
                /** Get approver id From tranpo_booking_approvals **/
                $approverId = \DB::table('transpo_booking_approvals')
                                    ->where('transpo_booking_id',$bookingId)
                                    ->where('approver_id', -1)
                                    ->select('transpo_approver_id')                                    
                                    ->first();

                /* Get users id from transpo_approver_users  */
                $approverUsers = \DB::table('transpo_approver_users')
                                        ->where('transpo_approver_id',$approverId->transpo_approver_id)
                                        ->select('user_id')
                                        ->get();

                /* create a list of the approver user */
                $approverUsersArray = $approverUsers->pluck('user_id');                                            
                                

                // Query Users table and get their email based on transpo_approver_users 
                $approverEmails = \DB::table('users')
                                        ->whereIn('id',$approverUsersArray)
                                        ->select('email')
                                        ->get();                

                // Create an array of emails
                $approverEmailArray = $approverEmails->pluck('email');
                
                //Query Booking Details
                $bookings = TranspoBooking::findOrFail($bookingId);

                // Send Email To First Approver
                Mail::to($approverEmailArray)->send(new TranspoNeedApproval($bookings));

                // Display Alert Message Successful
                \Flash::success('Successfully booked wait for approval');                

                return redirect()->route('transpo.index');  

            }

            \Flash::error('Contact Administrator no approvers set');

            return redirect()->route('transpo.index');  


        }    

        \Flash::error('Unable to book there is a current booking for this date');

        return redirect()->route('transpo.select.add',$id)->withInput();	

    }

}
