<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddReleaseDateFromTranspoBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transpo_bookings', function (Blueprint $table) {
            $table->datetime('release_date');
            $table->datetime('return_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transpo_bookings', function (Blueprint $table) {
            $table->dropColumn('release_date');
            $table->dropColumn('return_date');
        });
    }
}
