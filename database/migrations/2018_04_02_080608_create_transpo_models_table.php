<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTranspoModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transpo_models', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('transpo_make_id');
            $table->foreign('transpo_make_id')->references('id')->on('transpo_makes');
            $table->string('name');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('transpo_models');
    }
}
