<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTranspoTrafficViolationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transpo_traffic_violations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('transpo_vehicle_id');
            $table->foreign('transpo_vehicle_id')->references('id')->on('transpo_vehicles');
            $table->integer('driver_id');
            $table->date('violation_date');
            $table->string('location');
            $table->integer('transpo_type_of_violation_id');
            $table->double('amount');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('transpo_traffic_violations');
    }
}
