<div class="form-group">
  <label for="offense" class="col-md-2 control-label">Offense</label>
  <div class="col-md-10">
    {!! Form::text('name',null, ['class'=>'form-control']) !!}
  </div>
</div>
<div class="form-group">
  <label for="fine" class="col-md-2 control-label">Fine</label>
  <div class="col-md-10">
    {!! Form::text('fine',null, ['class'=>'form-control']) !!}
  </div>
</div>
<div class="form-group">
  <label for="black_points" class="col-md-2 control-label">Black Points</label>
  <div class="col-md-10">
    {!! Form::text('black_points',null, ['class'=>'form-control']) !!}
  </div>
</div>
<div class="form-group">
  <div class="col-md-offset-2 col-md-10">
    <button> Save</button>  
  </div>  
</div>