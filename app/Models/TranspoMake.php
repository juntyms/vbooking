<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TranspoMake extends Model
{
    protected $table = 'transpo_makes';

    protected $fillable = ['name'];
}
