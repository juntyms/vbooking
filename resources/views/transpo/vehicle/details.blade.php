@extends('transpo.base')

@section('breadcrumbs')
<section class="content-header">
  <h1>
    Tranporation System
    <small>Dashboard Panel</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>    
    <li><a href="{!! URL::route('transpo.index') !!}">Dashboard</a></li>
    <li><a href="{!! URL::route('transpo.vehicle.index') !!}">Vehicles</a></li>  
    <li><a href="#">Details</a></li>    
  </ol>
</section>
@endsection

@section('maincontent')
<section class="content">
	<div class="box">
		<div class="box-header with-border">
			<h3 class="box-title">Booking Request Details</h3>
			<div class="box-tools pull-right">
			  <!-- Buttons, labels, and many other things can be placed here! -->
			  <!-- Here is a label for example -->	      
			</div><!-- /.box-tools -->
		</div><!-- /.box-header -->
		<div class="box-body">	
			<div class="row">
				<div class="col-md-4">
					<img src="{!! asset($vehicle->photo) !!}" alt="Vehicle Photo">	
				</div>	  	
				<div class="col-md-8">
					<a href="{!! URL::route('transpo.vehicle.edit', $vehicle->id) !!}" class="btn btn-app"> <i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>						
						<dl class="dl-horizontal">
						  <dt>Malkiya Expiration Date : </dt><dd>{!! \Carbon\Carbon::parse($vehicle->malkiya_expiry)->format('d-M-Y') !!}</dd>
						  <dt>Status :</dt><dd>@if ($vehicle->available == 1) Available @else Unavailable @endif</dd>
						  <dt>Model :</dt><dd>{!! $vehicle->vehiclemodel->name !!}</dd>
						  <dt>Year :</dt><dd>{!! $vehicle->year !!}</dd>
						  <dt>Plate No. :</dt><dd>{!! $vehicle->plate_no !!}</dd>
						  <dt>Capacity :</dt><dd>{!! $vehicle->seating_capacity !!}</dd>
						  <dt>Current Kilometer</dt><dd>{!! $vehicle->km !!}</dd>
						</dl>				
				</div>
			</div>
			<!-- Maintenance Details -->
			<div class="panel panel-primary">
			  <!-- Default panel contents -->			  
			  <div class="panel-heading">Maintenance</div>
			  <div class="panel-body">
			    <a href="{!! URL::route('transpo.vehicle.maintenance',$vehicle->id) !!}" class="btn btn-app"> <i class="fa fa-file-text" aria-hidden="true"></i> Add</a>
			  </div>
			  	

			  <!-- Table -->
			  <table class="table table-bordered">
			    <tr class="bg-primary">
			    	<th>Maintenance Date</th>
			    	<th>KM</th>
			    	<th>Next Date</th>
			    	<th>Next KM</th>
			    </tr>
			    @foreach($maintenances as $maintenance)
			    <tr>
			    	<td>{!! \Carbon\Carbon::parse($maintenance->maintenance_date)->format('d-M-Y') !!}</td>
			    	<td>{!! $maintenance->km !!}</td>
			    	<td>{!! \Carbon\Carbon::parse($maintenance->next_maintenance_date)->format('d-M-Y') !!}</td>
			    	<td>{!! $maintenance->next_km !!}</td>
			    </tr>
			    @endforeach
			  </table>
			</div>
			<!-- Traffic Violation -->
			<div class="panel panel-info">
			  <!-- Default panel contents -->
			  <div class="panel-heading">Traffic Violation</div>
			  <!-- Table -->
			  <div class="panel-body">
			    <a href="{!! URL::route('transpo.vehicle.trafficviolation',$vehicle->id) !!}" class="btn btn-app"> <i class="fa fa-file-text" aria-hidden="true"></i> Add</a>
			  </div>
			  <table class="table">
			    <tr class="bg-info">
			    	<th>SN</th>
			    	<th> Date</th>
			    	<th> Driver</th>
			    	<th> Location</th>
			    	<th> Type of Violation</th>
			    	<th> Amount</th>
			    </tr>
			    @php
					$sn = 1;
			    @endphp
			    @foreach($violations as $violation)
			    <tr>
			    	<td>{!! $sn++ !!}</td>
			    	<td>{!! $violation->violation_date !!}</td>
			    	<td>@if (isset($violation->driver->fullname))
			    		{!! $violation->driver->fullname !!}	
			    		@endif		    		
			    	</td>
			    	<td>{!! $violation->location !!}</td>
			    	<td>{!! $violation->offense->name !!}</td>
			    	<td>{!! $violation->amount !!}</td>
			    </tr>
			    @endforeach
			  </table>
			</div>
			<!-- Accident History -->
			<div class="panel panel-warning">
			  <!-- Default panel contents -->
			  <div class="panel-heading">Accident History</div>
			  <div class="panel-body">
			    <a href="{!! URL::route('transpo.vehicle.accident',$vehicle->id) !!}" class="btn btn-app"> <i class="fa fa-file-text" aria-hidden="true"></i> Add</a>
			  </div>
			  <!-- Table -->
			  <table class="table">
			    <tr class="bg-warning">
			    	<th> Date of accident</th>
			    	<th> Driver</th>
			    	<th> Driver Email</th>
			    	<th> Description</th>			    	
			    </tr>
			    @foreach($accidents as $accident)
			    <tr>
			    	<td>{!! $accident->accident_date !!}</td>
			    	<td>{!! $accident->driver->fullname !!}</td>	
			    	<td>{!! $accident->driver->email !!}</td>		    	
			    	<td>{!! $accident->description !!}</td>
			    </tr>
			    @endforeach
			  </table>
			</div>
		</div>
	</div>
</section>
@endsection