<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TranspoMenuUser extends Model
{
    protected $table = 'transpo_menu_user';

    protected $fillable = ['transpo_menu_id','user_id'];

    public function user()
    {
        return $this->hasOne('App\Models\User','id','user_id');
    }

    public function menu()
    {
        return $this->hasOne('App\Models\TranspoMenu','id','transpo_menu_id');
    }
}
