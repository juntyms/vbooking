@extends('transpo.base')

@section('breadcrumbs')
<section class="content-header">
  <h1>
    Tranporation System
    <small>Dashboard Panel</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Settingst</a></li>
    <li><a href="#">Index</a></li>
  </ol>
</section>
@endsection

@section('maincontent')
<section class="content">
    <div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">User Menu</h3>
        <div class="box-tools pull-right">
        <!-- Buttons, labels, and many other things can be placed here! -->
        <!-- This will cause the box to collapse when clicked -->
        <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
        <!-- Here is a label for example -->
        </div>
        <!-- /.box-tools -->
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        {!! Form::open(['route'=>'transpo.setting.addmenu']) !!}
        <div class="col-md-5">
            <label for="">Menus</label>            
            {!! Form::select('transpo_menu_id',$menus,null, ['class'=>'form-control','size'=>'8']) !!}
        </div>
        <div class="col-md-1">
            <label for=""></label>
            <button class="btn btn-success btn-sm btn-block"> <i class="fa fa-arrow-circle-left" aria-hidden="true"></i> Add</button>
        </div>
        <div class="col-md-6">
        <label for="">Users</label>
            {!! Form::select('user_id',$users,null, ['class'=>'form-control select2','placeholder'=>'Select User . . . .']) !!}            
        </div>
        {!! Form::close() !!}
    </div>
    <!-- /.box-body -->
    <div class="box-footer">
        Transpo Menu Settings
    </div>
    <!-- box-footer -->
    </div>
    <!-- /.box -->

    <div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">User Menu List</h3>
        <div class="box-tools pull-right">
        <!-- Buttons, labels, and many other things can be placed here! -->
        <!-- This will cause the box to collapse when clicked -->
        <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
        
        <!-- Here is a label for example -->
        </div>
        <!-- /.box-tools -->
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <table class="table table-striped" id="menutables">
            <thead>
                <tr>
                    <th>Menu</th>
                    <th>User</th>
                    <th>User Email</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
            @foreach($menuUsers as $menuUser)
            <tr>
                <td>{!! $menuUser->menu->name_en !!}</td>
                <td>{!! $menuUser->user->fullname !!}</td>
                <td>{!! $menuUser->user->email !!}</td>
                <td>
                {!! Form::open(['route'=>['transpo.setting.delmenu',$menuUser->id]]) !!}
                    <button class="btn btn-sm btn-danger"> Delete</button>
                {!! Form::close() !!}
                </td>
            </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <!-- /.box-body -->
    <div class="box-footer">
        Transpo Menu Settings
    </div>
    <!-- box-footer -->
    </div>
    <!-- /.box -->
</section>
@endsection

@section('jscript')
{!! HTML::script('plugin/select/js/select2.min.js') !!}
<script>
    $(function () {
        //Initialize Select2 Elements
        $(".select2").select2();

        $('#menutables').DataTable(); //Full Feature
    });
</script>
@endsection
