<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TranspoMaintenance extends Model
{
    protected $table = 'transpo_maintenances';

    protected $fillable = ['transpo_vehicle_id','maintenance_date','km','next_maintenance_date','next_km','updated_by'];

    protected $dates = ['maintenance_date','next_maintenance_date']; 

    public function vehicle()
    {
        return $this->hasOne('App\TranspoVehicle','id','transpo_vehicle_id');
    }

    public function updatedBy()
    {
        return $this->hasOne('App\User','id','updated_by');
    }
}
