<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TranspoReturnRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'return_km' => 'required',
            'return_time' => 'required',
            'return_date' => 'required',
            'remarks' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'return_km.required' => 'Please Provide the Current Kilometer of the Vehicle',
            'return_time.required' => 'Please Provide the Time of Return',
            'return_date.required' => 'Please Provide the Date of Return'
        ];
    }
}
