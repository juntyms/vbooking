@extends('transpo.base')

@section('breadcrumbs')
<section class="content-header">
  <h1>
    Tranporation System
    <small>Dashboard Panel</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>    
    <li><a href="{!! URL::route('transpo.index') !!}">Dashboard</a></li>
    <li><a href="{!! URL::route('transpo.vehicle.index') !!}">Vehicles</a></li>  
    <li><a href="#">Maintenance</a></li>    
  </ol>
</section>
@endsection

@section('maincontent')
<section class="content">
  {!! Form::open(['route'=>['transpo.vehicle.postmaintenance',$id], 'class'=>'form-horizontal']) !!}
    <div class="form-group">
      <label for="maintenance_date" class="control-label col-md-2">Maintenance Date</label>
      <div class="col-md-3">
        <div class="input-group">
        {!! Form::date('maintenance_date', null, ['class'=>'form-control']) !!}
        <span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
        </div>
      </div>
    </div>
    <div class="form-group">
      <label for="km" class="control-label col-md-2">Current Kilometers</label>
      <div class="col-md-3">
        <div class="input-group">
          {!! Form::text('km', null, ['class'=>'form-control']) !!}
          <span class="input-group-addon"><i class="fa fa-tachometer" aria-hidden="true"></i></span>
          </div>
      </div>
    </div>
    <div class="form-group">
      <label for="nmd" class="control-label col-md-2">Next Maintenance Date</label>
      <div class="col-md-3">
        <div class="input-group">
          {!! Form::date('next_maintenance_date',null, ['class'=>'form-control']) !!}
          <span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
        </div>
      </div>
    </div>
    <div class="form-group">
      <label for="km" class="control-label col-md-2">Next Kilometers</label>
      <div class="col-md-3">
        <div class="input-group">
          {!! Form::text('next_km', null, ['class'=>'form-control']) !!}
          <span class="input-group-addon"><i class="fa fa-tachometer" aria-hidden="true"></i></span>
          </div>
      </div>
    </div>
    <div class="form-group">
      <div class="col-md-offset-2 col-md-10">
        <button class="btn btn-success"> <i class="fa fa-floppy-o" aria-hidden="true"></i> Save</button>  
        <a href="{!! URL::route('transpo.vehicle.details',$id) !!}" class="btn btn-danger"> <i class="fa fa-times" aria-hidden="true"></i> Cancel</a>
      </div>      
    </div>
  {!! Form::close() !!}
</section>
@endsection
