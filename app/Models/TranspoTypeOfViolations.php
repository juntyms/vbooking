<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TranspoTypeOfViolations extends Model
{
    protected $table = 'transpo_type_of_violations';

    protected $fillable = ['name','fine', 'black_points'];
}
