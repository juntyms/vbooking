<?php

namespace App\Http\Composers;

use Illuminate\Contracts\View\View;
use App\Models\TranspoMenuUser;
use Auth;

class TranspoNavigationComposer {

  public function nav(View $view)
  {
      $usermenus = TranspoMenuUser::where('user_id',Auth::user()->id)->get();

      $view->with('usermenus',$usermenus);
  }

}
