<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TranspoBookingApproval extends Model
{
    protected $table = 'transpo_booking_approvals';

    public function booking()
    {
    	return $this->belongsTo('App\TranspoBooking','transpo_booking_id','id');
    }

    public function approver()
    {
    	return $this->hasOne('App\User','id','approver_id');
    }

    public function approverdesc()
    {
    	return $this->hasOne('App\TranspoApprover','id','transpo_approver_id');
    }

    public function statusdesc()
    {
    	return $this->hasOne('App\TranspoStatus','id','transpo_status_id');
    }
}