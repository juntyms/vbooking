@extends('transpo.base')

@section('breadcrumbs')
<section class="content-header">
  <h1>
    Tranporation System
    <small>Approver</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Approver</a></li>        
  </ol>
</section>
@endsection

@section('maincontent')
<section class="content">
	<div class="box">
	  <div class="box-header with-border">	  	
	    <h3 class="box-title">Approver Type</h3>
	    <div class="box-tools pull-right">
	      <!-- Buttons, labels, and many other things can be placed here! -->
	      <!-- Here is a label for example -->
	      
	    </div><!-- /.box-tools -->
	  </div><!-- /.box-header -->
	  <div class="box-body">
	  	<a href="{!! URL::route('transpo.approver.add') !!}" class="btn btn-app"><i class="fa fa-plus-circle" aria-hidden="true"></i> Add</a>
	  	<table class="table table-bordered">
	  		<tr>
	  			<th>Approver Name</th>
	  			<th></th>
	  		</tr>
	    	@foreach($transpoApprovers as $transpoApprover)
			<tr>
				<td>{!! $transpoApprover->name !!}</td>
				<td><a href="{!! URL::route('transpo.approver.edit',$transpoApprover->id) !!}">Edit</a></td>
			</tr>
	    	@endforeach
	    </table>
	  </div><!-- /.box-body -->
	  <div class="box-footer">
	    
	  </div><!-- box-footer -->
	</div><!-- /.box -->
</section>
@endsection