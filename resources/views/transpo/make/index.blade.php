@extends('transpo.base')

@section('breadcrumbs')
<section class="content-header">
  <h1>
    Tranporation System
    <small>Dashboard Panel</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">make</a></li>
    <li><a href="#">list</a></li>
  </ol>
</section>
@endsection

@section('maincontent')
<section class="content">
	<a href="{!! URL::route('transpo.make.new') !!}" class="btn btn-app"> New Make </a>
  
  <table class="table table-striped">
    <tr>
      <th class="col-md-1">SN</th>
      <th> Make</th>
      <th> Action</th>
    </tr>
    @php
      $sn = 1
    @endphp
    @foreach($makes as $make)
    <tr>
      <td>{!! $sn++ !!}</td>
      <td>{!! $make->name !!}</td>
      <td><a href="{!! URL::route('transpo.make.edit',$make->id) !!}" class="btn btn-info btn-xs"> Edit</a>
          <a href="{!! URL::route('transpo.model.index',$make->id) !!}" class="btn btn-success btn-xs"> View Models</a>
          <a href="{!! URL::route('transpo.model.new', $make->id) !!}" class="btn btn-warning btn-xs"> New Model </a>
      </td>
    </tr>
    @endforeach
  </table>
</section>
@endsection