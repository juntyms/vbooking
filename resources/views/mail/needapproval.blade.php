<h3>Transportation Booking Request</h3>
<p><strong>Vehicle Name: </strong> {!! $booking->vehicle->vehiclemodel->name !!}</p>
<p><strong>Vehicle Plate: </strong> {!! $booking->vehicle->plate_no !!}</p>
<p><strong>Start Date: </strong> {!! \Carbon\Carbon::parse($booking->start_date)->format('d-M-Y') !!}</p>
<p><strong>End Date: </strong> {!!   \Carbon\Carbon::parse($booking->end_date)->format('d-M-Y') !!}</p>
<p><strong>Purpose: </strong> {!! $booking->purpose !!}</p>
<p><strong>Requested By: </strong> {!! $booking->requester->fullname !!} ( {!! $booking->requester->email !!} )</p>
<h3><a href="www.sct.edu.om/sctapps/public/transpo">Click Here to Open Application and Approve</a></h3>


