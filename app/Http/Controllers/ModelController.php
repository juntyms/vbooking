<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\TranspoModel;
use App\Models\TranspoMake;

class ModelController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index($makeId)
    {
    	$models = TranspoModel::where('transpo_make_id',$makeId)->get();

    	return view('transpo.model.index')
    				->with('models',$models)
    				->with('makeId',$makeId)
                    ->with('nav',2);
    }

    public function new($makeId)
    {
    	$makes = TranspoMake::all()->pluck('name','id');    	

    	$defaultMake = TranspoMake::findOrFail($makeId);

    	return view('transpo.model.new')
    				->with('makes',$makes)
    				->with('defaultMake',$defaultMake)
                    ->with('nav',2);
    }

    public function save(Request $request)
    {
    	TranspoModel::create($request->all());

    	\Flash::success('Model successfully created');

    	return redirect()->route('transpo.model.index',$request->transpo_make_id);
    }

    public function edit($id)
    {
    	$model = TranspoModel::findOrFail($id);

    	$makes = TranspoMake::all()->pluck('name','id');    

    	return view('transpo.model.edit')
    				->with('model',$model)
    				->with('makes',$makes)
                    ->with('nav',2);
    }

    public function update(Request $request, $id)
    {
    	$model = TranspoModel::findOrFail($id);

    	$model->update($request->all());

    	return redirect()->route('transpo.model.index',$model->transpo_make_id);
    }

    public function delete($id)
    {
    	$model = TranspoModel::findOrFail($id);

    	$makeId = $model->transpo_make_id;

    	$model->delete();

    	\Flash::error('Model Deleted');

    	return redirect()->route('transpo.model.index',$makeId);
    }
}
