<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TranspoModel extends Model
{
    protected $table = 'transpo_models';

    protected $fillable = ['transpo_make_id','name'];

    public function modelmake()
    {
    	return $this->belongsTo('App\Models\TranspoMake','transpo_make_id','id');
    }
}
