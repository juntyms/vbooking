@extends('transpo.base')

@section('breadcrumbs')
<section class="content-header">
  <h1>
    Tranporation System
    <small>Dashboard Panel</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Request</a></li>
  </ol>
</section>
@endsection

@section('maincontent')
<section class="content">
  <div class="panel panel-default">
    <div class="panel-heading">
      <h3 class="panel-title">Trasnportation Request</h3>
    </div>
    <div class="panel-body">      
      {!! Form::open(['class'=>'form-horizontal']) !!}
      <div class="form-group">
        {!! Form::label('vehicle','Vehicle', ['class'=>'control-label col-md-2']) !!}
        <div class="col-md-10">
          {!! Form::select('vehicle_id',[],null, ['class'=>'form-control']) !!}
        </div>
      </div>
      <div class="form-group">
        {!! Form::label('Date Start', 'Start Date', ['class'=>'control-label col-md-2']) !!}
        <div class="col-md-10">
          {!! Form::date('start_date', \Carbon\Carbon::now(), ['class'=>'form-control']) !!}
        </div>
      </div>
      <div class="form-group">
        {!! Form::label('End Date','End Date', ['class'=>'control-label col-md-2']) !!}
        <div class="col-md-10">
          {!! Form::date('end_date', \Carbon\Carbon::now(), ['class'=>'form-control']) !!}
        </div>
      </div>
      <div class="form-group">
        <div class="col-md-offset-2 col-md-10">
          <button class="btn btn-sm btn-primary"> Make Request</button>
        </div>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>
@endsection