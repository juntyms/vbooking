<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\TranspoVehicle;
use App\Models\TranspoBooking;
use Auth;
use App\Models\TranspoApproverUser;
use App\Models\TranspoBookingApproval;
use App\Models\TranpoBooking;

class TransportationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $isHOC = 0;
        $isHOD = 0;
        $isADFA = 0;
        $isDean = 0;
        $isHODAdmin = 0;

        $privilege = \DB::table('transpo_approver_users')
                        ->where('user_id',Auth::user()->id)
                        ->select('transpo_approver_id')
                        ->get();

        foreach($privilege as $priv)
        {
            if ($priv->transpo_approver_id == 1) {
                $isHOC = 1;
            }
            if ($priv->transpo_approver_id == 2) {
                $isHOD = 1;
            }
            if ($priv->transpo_approver_id == 3) {
                $isADFA = 1;
            }
            if ($priv->transpo_approver_id == 4) {
                $isDean = 1;
            }
            if ($priv->transpo_approver_id == 5) {
                $isHODAdmin = 1;
            }
        }

        $vehicles = TranspoVehicle::all();

        $reservations = TranspoBooking::where('requested_by',Auth::user()->id)->orderBy('transpo_status_id','asc')->get();        

        // All Approved Vehicle
        $forRelease = TranspoBooking::where('transpo_status_id',3)->get();

        // All released vehicle
        $forReturn = TranspoBooking::where('transpo_status_id',6)->orderBy('release_date','desc')->get();        

        $isApproverCollection = collect([]);  
        
        $isApprovers = TranspoApproverUser::where('user_id',Auth::user()->id)->get();

        foreach($isApprovers as $isApprover) {

            // identify if transpo_approver is general or specific
            $approver_identity = \DB::table('transpo_approvers')
                            ->where('id',$isApprover->transpo_approver_id)
                            ->first();

            if ($approver_identity->approver_type == 1) {
                // user department id should be the same as approver
                if ($isApprover->dept_id == Auth::user()->dept_id) {
                    $isApproverCollection->push($isApprover->transpo_approver_id);
                }
            } else {
                $isApproverCollection->push($isApprover->transpo_approver_id);
            }
        }        
//dd($isApproverCollection);
        $needapprovals = [];

        // check if the user is found on the approvers table as approver
        if ($isApprovers->count()) {


/*
            $needapproval = TranspoBooking::with(['requester' => function ($query) {
                                $query->where('dept_id','=',Auth::user()->dept_id);                                
                            }, 'approver' => function ($query2) {
                                $query2->where('approver_id','=',-1);
                                
                            }])
                            ->where('transpo_status_id','=',1)
                            ->get();       */

            $needapprovals = TranspoBookingApproval::whereIn('transpo_approver_id',$isApproverCollection)
                                    ->where('approver_id','-1')                                    
                                    ->get();
/*
            if ($needapproval->count()) {

                $forapproval = TranspoBookingApproval::where('transpo_status_id','=',1)
                                    ->where('approver_id',)
            } */

        }

        //dd($needapprovals);


    	return view('transpo.index')
                ->with('vehicles',$vehicles)
                ->with('reservations',$reservations)
                ->with('needapprovals',$needapprovals)    
                ->with('forRelease',$forRelease)   
                ->with('forReturn',$forReturn)
                ->with('isHOC',$isHOC)
                ->with('isHOD',$isHOD)
                ->with('isADFA',$isADFA)
                ->with('isDean',$isDean)
                ->with('isHODAdmin',$isHODAdmin)         
                ->with('nav',1);
    }


    public function request()
    {
    	return view('transpo.request')
                ->with('nav',1);
    }

    public function requestdetail($id)
    {
        $transpobooking = TranspoBooking::findOrFail($id);

        return view('transpo.requestdetail')
                    ->with('transpobooking',$transpobooking)
                    ->with('nav',1);
    }


}
