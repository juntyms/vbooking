@extends('transpo.base')

@section('breadcrumbs')
<section class="content-header">
  <h1>
    Tranporation System
    <small>Dashboard Panel</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Report</a></li>
    <li><a href="#">index</a></li>
  </ol>
</section>
@endsection

@section('maincontent')
<section class="content">
  <div class="box box-primary">
    <div class="box-header with-border">
      <h3 class="box-title"> Report</h3>
      <div class="box-tools pull-right">
        <!-- Buttons, labels, and many other things can be placed here! -->
        <!-- Here is a label for example -->
      </div>
      <!-- /.box-tools -->
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      {!! Form::open(['route'=> 'transpo.report.postrequest','class'=>'form-horizontal']) !!}
      <!-- Report Type -->
        <div class="form-group">
          <label for="reportType" class="control-label col-md-2">Report Type</label>
          <div class="col-md-10">
            <div class="radio">
              <label for="">
              <input type="radio" name="reportType" value="1" checked> Vehicle Request  
              </label>
            </div>
            <div class="radio">
              <label for="">
                <input type="radio" name="reportType" value="2" id=""> Maintenance Report
              </label>
            </div>
            <div class="radio">
              <label for="">
                <input type="radio" name="reportType" value="3" id=""> Violation Report  
              </label>
            </div>
            <div class="radio">
              <label for="">
                <input type="radio" name="reportType" value="4" id=""> Accident Report
              </label>
            </div>
            
          </div>
        </div>
      <!--/.Report Type -->
      <!-- Date range -->
      <div class="form-group">
        <label class="control-label col-md-2" >Date range:</label>
        <div class="col-md-6">
          <div class="input-group">
            <div class="input-group-addon">
              <i class="fa fa-calendar"></i>
            </div>
            <input type="text" name="date_range" class="form-control pull-right" id="reservation">
          </div>
          <!-- /.input group -->
        </div><!-- /.col-md-6 -->
      </div>
      <!-- /.form group -->
      <div class="form-group">
        <div class="col-md-offset-2 col-md-10">          
          <button class="btn btn-success"> Submit</button>
        </div>
      </div>
      {!! Form::close() !!}
    </div>
    <!-- /.box-body -->
    <div class="box-footer">
      Report Query
    </div>
    <!-- box-footer -->
  </div>
  <!-- /.box -->

</section>
@endsection

@section('jscript')
<script> 
  $(function() {
     //Date range picker
     $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )
  });
</script>
@endsection