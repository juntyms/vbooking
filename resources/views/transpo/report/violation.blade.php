@extends('transpo.base')

@section('breadcrumbs')
<section class="content-header">
  <h1>
    Tranporation System
    <small>Dashboard Panel</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Report</a></li>
    <li><a href="#">Traffic Violation</a></li>
  </ol>
</section>
@endsection

@section('maincontent')
<section class="content">
    <div class="box box-danger">
    <div class="box-header with-border">
        <h3 class="box-title">Traffic Violation Report</h3>
        <div class="box-tools pull-right">
        <!-- Buttons, labels, and many other things can be placed here! -->
        <!-- Here is a label for example -->
        </div>
        <!-- /.box-tools -->
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <table class="table table-bordered">
            <tr class="bg-danger">
                <th>Date of Violation</th>
                <th>Vehicle Name</th>
                <th>Plate No</th>
                <th>location</th>
                <th>Violation Type</th>
                <th>Amount</th>
                <th>Driver</th>
                <th>Updated By</th>                
            </tr>
            @foreach($violations as $violation)
            <tr>
                <td>{!! \Carbon\Carbon::parse($violation->violation_date)->format('d M Y') !!}</td>
                <td>{!! $violation->vehicle->vehiclemodel->name !!}</td>
                <td>{!! $violation->vehicle->plate_no !!}</td>
                <td>{!! $violation->location !!}</td>
                <td>{!! $violation->offense->name !!}</td>
                <td>{!! $violation->amount !!}</td>
                <td>{!! $violation->driver->fullname !!}</td>
                <td>{!! $violation->updatedBy->fullname !!}</td>
            </tr>
            @endforeach
        </table>
    </div>
    <!-- /.box-body -->
    <div class="box-footer">
        Booking Request List
    </div>
    <!-- box-footer -->
    </div>
    <!-- /.box -->    
    
</section>
@endsection