@extends('transpo.base')

@section('breadcrumbs')
<section class="content-header">
  <h1>
    Tranporation System
    <small>Violation Types</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>    
    <li><a href="{!! URL::route('transpo.vehicle.index') !!}">Violations</a></li>  
    <li><a href="#">Index</a></li>    
  </ol>
</section>
@endsection

@section('maincontent')
<section class="content">
  <div class="row">
    <div class="col-md-6">
      <div class="box box-solid box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Violation Types</h3>
          <div class="box-tools pull-right">
            <!-- Buttons, labels, and many other things can be placed here! -->
            <!-- Here is a label for example -->
            <span class="label label-primary">Label</span>
          </div><!-- /.box-tools -->
        </div><!-- /.box-header -->
        <div class="box-body">
          {!! Form::model($violation,[],['class'=>'form-horizontal']) !!}
              @include('transpo.violation._form')                        
          {!! Form::close() !!}
        </div><!-- /.box-body -->
        <div class="box-footer">
          Adding Violation Types
        </div><!-- box-footer -->
      </div><!-- /.box -->
    </div>
  </div>
  <div class="row">
    @include('transpo.violation._list')
  </div><!-- /.row -->

	
</section>
@endsection