@extends('transpo.base')

@section('breadcrumbs')
<section class="content-header">
  <h1>
    Tranporation System
    <small>Vehicle Return</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Booking</a></li>        
    <li><a href="#">Vehicle</a></li>        
    <li><a href="#">Return</a></li>        
  </ol>
</section>
@endsection

@section('maincontent')
	<section class="content">
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">Return Vehicle</h3>
        <div class="box-tools pull-right">
          <!-- Buttons, labels, and many other things can be placed here! -->
          <!-- Here is a label for example -->
          <span class="label label-primary">Label</span>
        </div><!-- /.box-tools -->
      </div><!-- /.box-header -->
      <div class="box-body">
        <!-- Vehicle Details -->        
        <!-- Submit Current KM -->
        {!! Form::open(['route'=>['transpo.booking.postreturn',$booking->id],'class'=>'form-horizontal','autocomplete'=>'off']) !!}
          <div class="row">
            <div class="col-md-6">              
              <div class="form-group">
                <label for="plateno" class="control-label col-md-2">Plate No</label>
                <div class="col-md-10">
                  {!! $booking->vehicle->plate_no !!}
                </div>
              </div>
              <div class="form-group">
                <label for="vehicle" class="control-label col-md-2">Vehicle</label>
                <div class="col-md-10">
                  {!! $booking->vehicle->vehiclemodel->name !!}
                </div>
              </div>
              <div class="form-group">
                <label for="return_date" class="control-label col-md-2">Return Date</label>
                <div class="col-md-10">   
                  <div class="input-group">
                    {!! Form::text('return_date', null, ['class'=>'form-control','id'=>'datepicker']) !!}
                    <div class="input-group-addon">
                      <i class="fa fa-calendar" aria-hidden="true"></i>
                    </div>
                  </div>           
                </div>
              </div>
              <div class="form-group">
                <label for="return_time" class="control-label col-md-2">Return Time</label>
                <div class="col-md-10">
                  <div class="bootstrap-timepicker">                              
                      <div class="input-group">
                        <input name="return_time" type="text" class="form-control timepicker">
                        <div class="input-group-addon">
                          <i class="fa fa-clock-o"></i>
                        </div>
                      </div><!-- /.input group -->                
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label for="km" class="control-label col-md-2">Current Kilometer</label>
                <div class="col-md-10">
                  {!! Form::text('return_km',null, ['class'=>'form-control']) !!}
                </div>
              </div>
            </div>
            <div class="col-md-6">        
              <div class="form-group">
                <label for="RequestedBy" class="control-label col-md-2">Requested By</label>
                <div class="col-md-10">
                  {!! $booking->requester->fullname !!}
                </div>
              </div>
              <div class="form-group">
                <label for="driver" class="control-label col-md-2">Driver</label>
                <div class="col-md-10">
                  {!! $booking->driver->fullname !!}
                </div>
              </div>
              <div class="form-group">
                <label for="start_date" class="control-label col-md-2">Start Date</label>
                <div class="col-md-10">
                  {!! \Carbon\Carbon::parse($booking->start_date)->format('d-M-Y') !!}
                </div>
              </div>
              <div class="form-group">
                <label for="end_date" class="control-label col-md-2">End Date</label>
                <div class="col-md-10">
                  {!! \Carbon\Carbon::parse($booking->end_date)->format('d-M-Y') !!}
                </div>
              </div> 
              <div class="form-group">
                <label for="purpose" class="control-label col-md-2">Purpose</label>
                <div class="col-md-10">
                  {!! $booking->purpose !!}
                </div>
              </div>
            </div>            
          </div>
          <div class="row">     
            <div class="col-md-8">
              <div class="form-group">
                <label for="remarks" class="control-label col-md-2"> Remarks / Comments</label>
                <div class="col-md-10">
                  {!! Form::textarea('remarks',null, ['class'=>'form-control']) !!}
                </div>
              </div>                                
              <div class="form-group">
                <div class="col-md-offset-2 col-md-10">
                  <button class="btn btn-success"> <i class="fa fa-reply" aria-hidden="true"></i> Return</button>
                </div>
              </div>
            </div>            
            <div class="col-md-4">
              <div class="form-group">
                <label for="photo" class="control-label col-md-2">&nbsp;</label>
                <div class="col-md-10">
                  <img src="{!! asset($booking->vehicle->photo) !!}">
                </div>
              </div>
            </div>         

          </div>
        {!! Form::close() !!}
      </div><!-- /.box-body -->
      <div class="box-footer">
        Vehicle Details to return
      </div><!-- box-footer -->
    </div><!-- /.box -->
  </section>
@endsection  

@section('jscript')
  <script>
   $('#datepicker').datepicker({
      autoclose: true
    });

   //Timepicker
    $(".timepicker").timepicker({
      showInputs: false
    });
   </script>
@endsection