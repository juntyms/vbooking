<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TranspoApprover extends Model
{
    protected $table ='transpo_approvers';

    protected $fillable = ['name'];
}
