<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddReleaseByFromTranspoBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transpo_bookings', function (Blueprint $table) {
            $table->integer('release_by')->nullable();
            $table->integer('accepted_by')->nullable();
            $table->string('release_remarks')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transpo_bookings', function (Blueprint $table) {
            $table->dropColumn('release_by');
            $table->dropColumn('accepted_by');
            $table->dropColumn('release_remarks');
        });
    }
}
