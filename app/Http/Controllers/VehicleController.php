<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\TranspoVehicle;
use App\Models\TranspoMake;
use Auth;
use App\Models\TranspoMaintenance;
use App\Models\TranspoTrafficViolation;
use App\Models\TranspoAccidentHistory;
use App\Models\User;

class VehicleController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
    	$vehicles = TranspoVehicle::all();

    	return view('transpo.vehicle.index')
    				->with('vehicles',$vehicles)
                    ->with('nav',3);
    }

    public function add()
    {
    	$makes = TranspoMake::pluck('name','id');

    	$models = [];

    	return view('transpo.vehicle.add')
    			->with('makes',$makes)
    			->with('models',$models)
                ->with('nav',3);
    }

    public function save(Request $request)
    {
        TranspoVehicle::create($request->all());
        
        \Flash::success('Succcessfully Add Vehicle');

        return redirect()->route('transpo.vehicle.index');
    }

    public function edit($id)
    {
        $vehicle = TranspoVehicle::findOrFail($id);

        return view('transpo.vehicle.edit')
                    ->with('vehicle',$vehicle)
                    ->with('nav',3);
    }

    public function update(Request $request, $id)
    {
        $vehicle = TranspoVehicle::findOrFail($id);

        $vehicle->update($request->all());

        \Flash::success('Succcessfully Updated Vehicle');

        return redirect()->route('transpo.vehicle.index');
    }

    /*  
    * Get the Details of the vehicle
    */
    public function details($id)
    {
        //$vehicle = \DB::table('transpo_vehicles')->where('id',$id)->first();

        $vehicle = TranspoVehicle::findOrFail($id);        

        $maintenances = TranspoMaintenance::where('transpo_vehicle_id',$id)->get();

        $violations = TranspoTrafficViolation::where('transpo_vehicle_id',$id)->get();

        $accidents = TranspoAccidentHistory::where('transpo_vehicle_id',$id)->get();

        return view('transpo.vehicle.details')
                    ->with('vehicle',$vehicle)
                    ->with('maintenances',$maintenances)                    
                    ->with('violations',$violations)
                    ->with('accidents',$accidents)
                    ->with('nav',3);
    }

    public function maintenance($id)
    {       

        return view('transpo.vehicle.maintenance')
                    ->with('nav',3)
                    ->with('id',$id);
    }

    public function postmaintenance(Request $request, $id)
    {
        $request['transpo_vehicle_id'] = $id;

        $request['updated_by'] = Auth::user()->id;

        TranspoMaintenance::create($request->all());

        \Flash::success('Maintenenace Record Updated');

        return redirect()->route('transpo.vehicle.details',$id);
    }

    public function accident($id)
    {
        $accidents = TranspoAccidentHistory::where('transpo_vehicle_id',$id)->get();

        $drivers = User::where('isStaff',1)->pluck('fullname','id');

        return view('transpo.vehicle.accident')
                    ->with('nav',3)
                    ->with('id',$id)
                    ->with('accidents',$accidents)
                    ->with('drivers',$drivers);

    }

    public function postaccident(Request $request, $id)
    {
        $request['updated_by'] = Auth::user()->id;
        
        $request['transpo_vehicle_id'] = $id;

        TranspoAccidentHistory::create($request->all());

        \Flash::success('Accident Details Added');

        return redirect()->route('transpo.vehicle.details',$id);
    }

}
