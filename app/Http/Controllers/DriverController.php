<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\TranspoDriver;
use App\Models\User;

class DriverController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
    	//list of drivers
    	$drivers = TranspoDriver::all();

    	return view('transpo.driver.index')
    				->with('drivers',$drivers)
                    ->with('nav',4);;
    }

    public function add()
    {
    	$users = User::all();

    	return view('transpo.driver.add')
                    ->with('users',$users)
                    ->with('nav',4);
    }

    public function save($id)
    {
    	$driver = TranspoDriver::find($id);

    	if (empty($driver)) {
    		TranspoDriver::create(['user_id'=>$id,'status'=>'0']);
    	}

    	\Flash::success('Driver Added');

    	return redirect()->route('transpo.driver.index');
    }

    public function remove($id)
    {
    	$driver = TranspoDriver::findOrFail($id);

    	$driver->delete();

    	\Flash::error('Driver Deleted');

    	return redirect()->route('transpo.driver.index');
    }

    
}
