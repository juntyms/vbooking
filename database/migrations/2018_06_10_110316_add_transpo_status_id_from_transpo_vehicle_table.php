<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTranspoStatusIdFromTranspoVehicleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transpo_vehicles', function (Blueprint $table) {
            $table->integer('transpo_vehicle_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transpo_vehicles', function (Blueprint $table) {
            $table->dropColumn('transpo_vehicle_id');
        });
    }
}
