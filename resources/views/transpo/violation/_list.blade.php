    <div class="col-md-12">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">List of Violations</h3>
          <div class="box-tools pull-right">
            <!-- Buttons, labels, and many other things can be placed here! -->
            <!-- Here is a label for example -->
            <span class="label label-primary">Label</span>
          </div><!-- /.box-tools -->
        </div><!-- /.box-header -->
        <div class="box-body">
          <table class="table table-bordered">
            <tr>
              <th>SN</th>
              <th>Offense</th>
              <th>Fine</th>
              <th>Points</th>
            </tr>
            @php
              $sn = 1;
            @endphp
            @foreach($type_of_violations as $type)
            <tr>
              <td>{!! $sn++ !!}</td>
              <td>{!! $type->name !!}</td>
              <td>{!! $type->fine !!}</td>
              <td>{!! $type->black_points !!}</td>
            </tr>
            @endforeach
          </table>
        </div><!-- /.box-body -->
        <div class="box-footer">
          List of Violations
        </div><!-- box-footer -->
      </div><!-- /.box -->
    </div><!-- /.col-md-12 -->
