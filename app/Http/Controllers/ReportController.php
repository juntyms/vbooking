<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\TranspoBooking;
use App\Models\TranspoMaintenance;
use App\Models\TranspoTrafficViolation;
use App\Models\TranspoAccidentHistory;

class ReportController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('transpo.report.index')
                    ->with('nav',8);
    }

    public function request()
    {
        return view('transpo.report.request')
                    ->with('nav',8);
    }    

    public function postrequest(Request $request)
    {
        
        /* Separate the date range */
        $date_range = explode(" - ",$request->date_range);

        /* Instantiate Start Date to Carbon Instance */
        $start_date = \Carbon\Carbon::parse($date_range[0]);
        /** Instantiate End Date to Carbon Instance */
        $end_date = \Carbon\Carbon::parse($date_range[1])->addDay();

        /** Check Report Type to Query */
        switch ($request->reportType) {
            case '1': /** Vehicle Requests */
                    $bookingRequests = TranspoBooking::whereBetween('created_at',[$start_date,$end_date])->get();
            
                    if ($bookingRequests->isEmpty()) {
                        \Flash::error('No Booking Record Found');
            
                        return redirect()->route('transpo.report.index')->withInput();
                        
                    }
            
                    return view('transpo.report.vehiclerequest')
                                    ->with('nav',8)
                                    ->with('bookingRequests',$bookingRequests);
                break;
            case '2' : /** Vehicle Maintenance Report */
                    $maintenances = TranspoMaintenance::whereBetween('created_at',[$start_date, $end_date])->get();

                    if ($maintenances->isEmpty()) {

                        \Flash::error(' No Maintenance Record Found');

                        return redirect()->route('transpo.report.index')->withInput();
                    }

                    return view('transpo.report.maintenance')
                                    ->with('nav', 8)
                                    ->with('maintenances',$maintenances);
                break;
            case '3': /** Violation Report */
                    $violations = TranspoTrafficViolation::whereBetween('violation_date', [$start_date, $end_date])->get();

                    if ($violations->isEmpty()) {

                        \Flash::error(' No Traffic Violation Found');

                        return redirect()->route('transpo.report.index')->withInput();
                    }

                    return view('transpo.report.violation')
                                ->with('nav',8)
                                ->with('violations',$violations);
                    
                break;
            case '4': /** Accident Report */

                    $accidents = TranspoAccidentHistory::whereBetween('accident_date',[$start_date,$end_date])->get();

                    if ($accidents->isEmpty()) {
                        
                        \Flash::error(' No Accident Record Found');

                        return redirect()->route('transpo.report.index')->withInput();
                    }

                    return view('transpo.report.accident')
                                ->with('nav',8)
                                ->with('accidents',$accidents);
                break;
            default:
                
                break;
        }

        
    }
}
