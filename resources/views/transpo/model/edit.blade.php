@extends('transpo.base')

@section('breadcrumbs')
<section class="content-header">
  <h1>
    Tranporation System
    <small>Dashboard Panel</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Model</a></li>
    <li><a href="#">Edit</a></li>
  </ol>
</section>
@endsection

@section('maincontent')
<section class="content">
	
	{!! Form::model($model, ['route'=>['transpo.model.update',$model->id],'class'=>'form-horizontal']) !!}
		<div class="col-md-6">
			<div class="form-group">
				{!! Form::label('transpo_make','Make Name', ['class'=>'control-label col-md-2']) !!}
				<div class="col-md-10">
					{!! Form::select('transpo_make_id',$makes , null, ['class'=>'form-control']) !!}
				</div>
			</div>
			@include('transpo.model._form',['submitText'=>' Update'])
		</div>
	{!! Form::close() !!}
</section>
@endsection