<?php

namespace App\Http\Composers;

use Illuminate\Contracts\View\View;
use App\HrPosDeptApprover;

class PosDepAppComposer {

  public function posdepappform(View $view)
  {
      $positions = \DB::table('positions')
                      ->pluck('position','id');

      $depts = \DB::table('depts')
                    ->pluck('name','id');

      $posdepts = HrPosDeptApprover::all();

      $leaveTypes = \DB::table('hr_leave_types')
                        ->pluck('name','id');

      $view->with('positions',$positions)
          ->with('depts',$depts)
          ->with('posdepts',$posdepts)
          ->with('leaveTypes',$leaveTypes);
  }

}
