<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\TranspoMake;

class TranspoMakesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('transpo_makes')->delete();
        TranspoMake::create(['name'=>'Mitsubishi']);
        TranspoMake::create(['name'=>'Toyota']);
        TranspoMake::create(['name'=>'Hyundai']);
        TranspoMake::create(['name'=>'Nissan']);
        TranspoMake::create(['name'=>'Honda']);
    }
}
