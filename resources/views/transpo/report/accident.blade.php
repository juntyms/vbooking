@extends('transpo.base')

@section('breadcrumbs')
<section class="content-header">
  <h1>
    Tranporation System
    <small>Dashboard Panel</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Report</a></li>
    <li><a href="#">Traffic Accident</a></li>
  </ol>
</section>
@endsection

@section('maincontent')
<section class="content">
    <div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Traffic Accident Report</h3>
        <div class="box-tools pull-right">
        <!-- Buttons, labels, and many other things can be placed here! -->
        <!-- Here is a label for example -->
        </div>
        <!-- /.box-tools -->
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <table class="table table-bordered">
            <tr class="bg-danger">
                <th>Date of Accident</th>
                <th>Vehicle Name</th>
                <th>Plate No</th>
                <th>Description</th>
                <th>Driver</th>
                <th>Updated By</th>                
            </tr>
            @foreach($accidents as $accident)
            <tr>
                <td>{!! \Carbon\Carbon::parse($accident->accident_date)->format('d M Y') !!}</td>
                <td>{!! $accident->vehicle->vehiclemodel->name !!}</td>
                <td>{!! $accident->vehicle->plate_no !!}</td>
                <td>{!! $accident->description !!}</td>                
                <td>{!! $accident->driver->fullname !!}</td>
                <td>{!! $accident->updatedBy->fullname !!}</td>
            </tr>
            @endforeach
        </table>
    </div>
    <!-- /.box-body -->
    <div class="box-footer">
        Accident History List
    </div>
    <!-- box-footer -->
    </div>
    <!-- /.box -->    
    
</section>
@endsection