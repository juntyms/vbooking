<?php

namespace App\Providers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use Auth;

class ViewComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->composeNavigation();

    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    private function composeNavigation()
    {
        //view()->composer('appraisal.navbar','App\Http\Composers\NavigationComposer@compose');
        //view()->composer('appraisal.assignment.create','App\Http\Composers\NavigationComposer@loadDept');
        //view()->composer('main._mainheader', 'App\Http\Composers\NavigationComposer@compose');
        //view()->composer('main._mainsidebar', 'App\Http\Composers\NavigationComposer@compose');
        //view()->composer('store.base', 'App\Http\Composers\NavigationComposer@storePrivilege');
/*
        View::composer('appraisal.navbar','App\Http\Composers\NavigationComposer@compose');
        View::composer('appraisal.assignment.create','App\Http\Composers\NavigationComposer@loadDept');
        View::composer('main._mainheader', 'App\Http\Composers\NavigationComposer@compose');
        View::composer('main._mainsidebar', 'App\Http\Composers\NavigationComposer@compose');
        View::composer('store.base', 'App\Http\Composers\NavigationComposer@storePrivilege');
        View::composer('customreport.index', 'App\Http\Composers\NavigationComposer@compose');
        View::composer('hr.base', 'App\Http\Composers\NavigationComposer@hrPrivilege');
        View::composer('hr.base', 'App\HTtp\Composers\NavigationComposer@hrMenu');
        View::composer('hr.employee._form', 'App\Http\Composers\HrComposer@employeeForm');
        View::composer('hr.dashboard', 'App\Http\Composers\NavigationComposer@hrPrivilege');
        View::composer('hr.leave.configure.approver.index','App\Http\Composers\PosDepAppComposer@posdepappform');
        View::composer('hr.leave.configure.approver.edit','App\Http\Composers\PosDepAppComposer@posdepappform');
        View::composer('bs.base', 'App\Http\Composers\NavigationComposer@bsPrivilege');
        View::composer('bs.booking.add', 'App\Http\Composers\NavigationComposer@bsPrivilege'); */
        View::composer('transpo.nav', 'App\Http\Composers\TranspoNavigationComposer@nav');
    }
}
