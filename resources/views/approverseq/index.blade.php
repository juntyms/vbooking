@extends('transpo.base')

@section('breadcrumbs')
<section class="content-header">
  <h1>
    Tranporation System
    <small>Approver Sequence</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Approver Sequence</a></li>        
  </ol>
</section>
@endsection

@section('maincontent')
<section class="content">
	<div class="box box-solid box-warning">
		<div class="box-header with-border">
		    <h3 class="box-title">Approver Sequence</h3>
		    <div class="box-tools pull-right">
		      <!-- Buttons, labels, and many other things can be placed here! -->
		      <!-- Here is a label for example -->	      
		    </div><!-- /.box-tools -->
		</div><!-- /.box-header -->
		<div class="box-body">
			<a href="{!! URL::route('transpo.approver.seq.add') !!}" class="btn btn-app"><i class="fa fa-plus-circle" aria-hidden="true"></i> Assign</a>
		  <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
			@foreach($depts as $dept)	
		  <div class="panel panel-default">
		    <div class="panel-heading" role="tab" id="heading{!! $dept->id !!}">
		      <h4 class="panel-title">
		        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse{!! $dept->id !!}" aria-expanded="true" aria-controls="collapse{!! $dept->id !!}">
		          {!! $dept->name !!}
		        </a>
		      </h4>
		    </div>
		    <div id="collapse{!! $dept->id !!}" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading{!! $dept->id !!}">
		      <div class="panel-body">
		      	@if (count($dept->transpoapproverseq) > 0)
				<table class="table table-bordered">
						<tr class="bg-orange">
							<th>Role</th>
							<th>Approver Sequence</th>
							<th></th>
							<th></th>
						</tr>
					@foreach($dept->transpoapproverseq as $approver)
						<tr>
							<td>{!! $approver->role->name !!}</td>
							<td>{!! $approver->seq !!}</td>
							<td>
								@if ($approver->seq == 1)
									<a href="{!! URL::route('transpo.approver.seq.down', $approver->id) !!}"><i class="fa fa-arrow-down" aria-hidden="true"></i></a>					
								@elseif ($approver->seq == count($dept->transpoapproverseq))
									<a href="{!! URL::route('transpo.approver.seq.up',$approver->id) !!}"><i class="fa fa-arrow-up" aria-hidden="true"></i></a>
								@else
									<a href="{!! URL::route('transpo.approver.seq.up',$approver->id) !!}"><i class="fa fa-arrow-up" aria-hidden="true"></i></a>
									<a href="{!! URL::route('transpo.approver.seq.down', $approver->id) !!}"><i class="fa fa-arrow-down" aria-hidden="true"></i></a>					
								@endif

							</td>
							<td><a href="{!! URL::route('transpo.approver.seq.delete',$approver->id) !!}" class="text-danger"><i class="fa fa-window-close" aria-hidden="true"></i></a></td>
						</tr>
					@endforeach
				</table>		        
				@endif
		      </div>
		    </div>
		  </div>
			@endforeach
		</div>
		</div>
	</div>
</section>
@endsection